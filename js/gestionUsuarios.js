$(document).ready(function() {
    menu();
    footer();
    buscadorUsuarios();
    buscadorUsuariosBaja();

    formUpdateUsuarios();

    $(buscar_datos());

    buscar_datos();

    $(buscar_datos_baja());

    buscar_datos_baja();
});

function menu() {
    $.ajax({
        url: 'menu',
        success: function(respuesta) {
            $('.menu').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function footer() {
    $.ajax({
        url: 'footer',
        success: function(respuesta) {
            $('.footer').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function formUsuarios() {
    ocultar =
        '<h1 class="titulo" id="mostrarFormUsuarios">Gestión de Usuarios <i class="fas fa-minus-circle" onclick="ocultarFormUsuarios()" style="color: red;"></i></h1>';
    $.ajax({
        url: 'formUsuarios',
        success: function(respuesta) {
            $('.formUsuarios').html(respuesta)
            $('#mostrarFormUsuarios').html(ocultar);
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function ocultarFormUsuarios() {
    mostrar =
        '<h1 class="titulo" id="mostrarFormUsuarios">Gestión de Usuarios <i class="fas fa-plus-circle" onclick="formUsuarios()" style="color: #007bff;"></i></h1>';
    $('.formUsuarios').html("");
    $('#mostrarFormUsuarios').html(mostrar);
}


function buscadorUsuarios() {
    $.ajax({
        url: 'buscadorUsuarios',
        success: function(respuesta) {
            $('.buscadorUsuarios').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function buscadorUsuariosBaja() {
    $.ajax({
        url: 'buscadorUsuariosBaja',
        success: function(respuesta) {
            $('.buscadorUsuariosBaja').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function formUpdateUsuarios() {
    $.ajax({
        url: 'formUpdateUsuarios',
        success: function(respuesta) {
            $('.formUpdateUsuarios').html(respuesta)

            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function ocultarFormUpdateUsuarios() {
    $('.formUpdateUsuarios').html("");
}

function buscar_datos(consulta) {
    $.ajax({
            url: 'getTableUsuarios',
            type: 'POST',
            dataType: 'html',
            data: { consulta: consulta },
        })
        .done(function(respuesta) {
            $('.getTableUsuarios').html(respuesta);

        })
        .fail(function() {
            console.log("error");
        })
    return false;
}

$(document).on('keyup', '#caja_busqueda', function() {
    var valor = $(this).val();
    if (valor != "") {
        buscar_datos(valor);
    } else {
        buscar_datos();
    }
})

function buscar_datos_baja(consulta) {
    $.ajax({
            url: 'getTableUsuariosBaja',
            type: 'POST',
            dataType: 'html',
            data: { consulta: consulta },
        })
        .done(function(respuesta) {
            $('.getTableUsuariosBaja').html(respuesta);

        })
        .fail(function() {
            console.log("error");
        })
    return false;
}

$(document).on('keyup', '#caja_busqueda_baja', function() {
    var valor = $(this).val();
    if (valor != "") {
        buscar_datos(valor);
    } else {
        buscar_datos();
    }
})

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 21.1713976,
            lng: -86.8712448
        },
        zoom: 12
    });
    $.ajax({
        url: 'getUsuarios',
        success: function(respuesta) {
            markers = JSON.parse(respuesta);
            for (const prop in markers) {
                // console.log(`markers.${prop} = ${markers[prop]}`);
                // console.log(parseFloat(markers[prop].latitud), parseFloat(markers[prop].longitud))
                addMarker(markers[prop]);
            }
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }

    });

    // The location of Uluru
    // The map, centered at Uluru
    function addMarker(props) {
        // console.log(props.latitud);
        //return;
        var marker = new google.maps.Marker({
            position: {
                lat: parseFloat(props.latitud),
                lng: parseFloat(props.longitud)
            },
            map: map,
            title: 'Hello World!',

            icon: 'styles/images/palmera.png',
            content: '<p>' + props.nombreUsuario + '</p>'
                // content: '<a href="/playas/' + props.id + '"> hola' + props.nombre + '</a>',
                //icon:props.iconImage
        });

    }

}





function saveUsuario() {

    name = $('#name').val();
    lastName = $('#lastName').val();
    age = $('#age').val();
    mobile = $('#mobile').val();
    email = $('#email').val();
    password = $('#password').val();
    passwordV = $('#passwordV').val();

    if (name === "" || lastName === "" || age === "" || mobile === "" || email === "" || password === "") {

        Swal.fire({
            icon: 'error',
            title: 'Todos los campos son obligatorios',
        })
        return false;
    }

    if (name.length > 50) {
        longWord("nombre");
        return false;
    }
    if (lastName.length > 50) {
        longWord("apellido");
        return false;
    }
    if (age.length > 50) {
        longWord("edad");
        return false;
    }
    if (mobile.length > 50) {
        longWord("celular");
        return false;
    }
    if (email.length > 50) {
        longWord("correo electrónico");
        return false;
    }

    if (isNaN(age)) {

        Swal.fire({
            icon: 'error',
            title: 'La edad ingresada no es un número',

        })
        return false;
    }

    if (!checkEmail(email)) {
        Swal.fire({
            icon: 'error',
            title: 'el correo electrónico no es valido',
        })
        return false;
    }

    if (password.length > 50) {
        longWord("contraseña");
        return false;
    }

    if (password != passwordV) {

        Swal.fire({
            icon: 'error',
            title: 'Las contraseñas no coinciden',

        })
        return false;
    }


    saveRegistro();
}

function saveRegistro() {
    var datos = $('#formUsuario').serialize();

    $.post("saveUsuario", datos).done(function(data) {
        var variable = 2;
        variable = data;

        if (variable == 1) {

            Swal.fire({
                icon: 'success',
                title: 'Se ha guardado con exito. ',
            })
        } else if (variable == 2) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error, favor de recargar la página.',
            })
        } else if (variable == 3) {
            Swal.fire({
                icon: 'error',
                title: 'El usuario ya existe, ingresa otro.',
            })
        } else if (variable == 4) {
            Swal.fire({
                icon: 'error',
                title: 'El correo ya existe, ingresa otro.',
            })
        }

    });
    return false;
}

function longWord(area) {
    Swal.fire({
        icon: 'error',
        title: 'La palabra ingresada en el campo ' + area + ' es muy larga',

    })
}

function shortWord(area) {
    Swal.fire({
        icon: 'error',
        title: 'La palabra ingresada en el campo ' + area + ' es muy corta',

    })
}

function checkEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}


function modificarUsuario(id) {
    $.ajax({
            url: 'formUpdateUsuarios',
            type: 'POST',
            dataType: 'html',
            data: { idUser: id },
        })
        .done(function(respuesta) {
            $('.formUpdateUsuarios').html(respuesta);

        })
        .fail(function() {
            console.log("error");
        })
    return false;
}

function updateUsuario() {
    var datos = $('#formUpdateUsuario').serialize();

    $.post("updateUsuario", datos).done(function(data) {
        var variable = 2;
        variable = data;
        if (variable == 1) {
            Swal.fire({
                icon: 'success',
                title: 'Se ha guardado con exito. ',
            })
        } else {
            Swal.fire({
                icon: 'error',
                title: 'No se ha podido actualizar. ',
            })
        }
        buscar_datos();

    });
    return false;
}

function deleteUsuario(idUsuario) {
    console.log("hola");
    console.log(idUsuario);
    $.ajax({
            url: 'deleteUsuario',
            type: 'POST',
            dataType: 'html',
            data: { idUser: idUsuario },
        })
        .done(function(respuesta) {
            if (respuesta == 1) {
                Swal.fire({
                    icon: 'success',
                    title: 'Se ha guardado con exito. ',
                })
                $('.formUpdateUsuarios').html("");
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Ha ocurrido un error. ',
                })
            }
            buscar_datos();
        })
        .fail(function() {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error. ',
            })
        })
}

function eliminar(idUsuario, userName) {
    Swal.fire({
        title: 'Esta seguro de dar de baja al usuario: ' + userName,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {

            $.ajax({
                    url: 'deleteLogicoUsuario',
                    type: 'POST',
                    dataType: 'html',
                    data: { idUser: idUsuario },
                })
                .done(function(respuesta) {
                    if (respuesta == 1) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Se ha eliminado con exito. ',
                        })
                        $('.formUpdateUsuarios').html("");
                    } else if (respuesta == 3) {
                        Swal.fire({
                            icon: 'error',
                            title: 'No se puede dar de baja debido a que es un administrador. ',
                        })
                        $('.formUpdateUsuarios').html("");
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Ha ocurrido un error. ',
                        })
                    }
                    buscar_datos();
                    buscar_datos_baja();

                })
                .fail(function() {
                    Swal.fire({
                        icon: 'error',
                        title: 'Ha ocurrido un error. ',
                    })
                })

            return false;
        }
    });


};

function crearLogicoUsuario(idUsuario, userName) {
    Swal.fire({
        title: 'Esta seguro de dar de alta al usuario: ' + userName,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {

            $.ajax({
                    url: 'createLogicoUsuario',
                    type: 'POST',
                    dataType: 'html',
                    data: { idUser: idUsuario },
                })
                .done(function(respuesta) {
                    if (respuesta == 1) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Se ha eliminado con exito. ',
                        })
                        $('.formUpdateUsuarios').html("");
                    } else if (respuesta == 3) {
                        Swal.fire({
                            icon: 'error',
                            title: 'No se puede dar de baja debido a que es un administrador. ',
                        })
                        $('.formUpdateUsuarios').html("");
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Ha ocurrido un error. ',
                        })
                    }
                    buscar_datos();
                    buscar_datos_baja();
                })
                .fail(function() {
                    Swal.fire({
                        icon: 'error',
                        title: 'Ha ocurrido un error. ',
                    })
                })

            return false;
        }
    });


};

function cambiarClave() {
    clave = document.getElementById("claven").value;
    claveV = document.getElementById("rclaven").value;
    if (clave === "" || claveV === "") {

        Swal.fire({
            icon: 'error',
            title: 'Los campos de contraseña son obligatorios. ',
        })
        return false;
    }
    if (claveV != clave) {

        Swal.fire({
            icon: 'error',
            title: 'las contraseñas no coinciden. ',
        })
        return false;
    } else {
        var datos = $('#cambiarContra').serialize();
        $.post("cambiarClaveUsuario", datos).done(function(data) {
            variable = data;
            if (data == 1) {
                Swal.fire({
                    icon: 'success',
                    title: 'Se ha actualizado con exito. ',
                })
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Ha ocurrido un error. ',
                })
            }
        });
        return false;
    }

}