$(document).ready(function() {
    menu();
    footer();

    formularioRegistro();
    formulariologin();



});

function menu() {
    $.ajax({
        url: 'menu',
        success: function(respuesta) {
            $('.menu').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });

}

function footer() {
    $.ajax({
        url: 'footer',
        success: function(respuesta) {
            $('.footer').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function formularioRegistro() {
    $.ajax({
        url: 'formRegistro',
        success: function(respuesta) {
            $('.registro').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function formulariologin() {
    $.ajax({
        url: 'formLogin',
        success: function(respuesta) {
            $('.login').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function formRegistro() {
    name = $('#name').val();
    lastName = $('#lastName').val();
    age = $('#age').val();
    mobile = $('#mobile').val();
    email = $('#email').val();
    password = $('#password').val();
    passwordV = $('#passwordV').val();

    if (name === "" || lastName === "" || age === "" || mobile === "" || email === "" || password === "") {

        Swal.fire({
            icon: 'error',
            title: 'Todos los campos son obligatorios',
        })
        return false;
    }

    if (name.length > 50) {
        longWord("nombre");
        return false;
    }
    if (lastName.length > 50) {
        longWord("apellido");
        return false;
    }
    if (age.length > 50) {
        longWord("edad");
        return false;
    }
    if (mobile.length > 50) {
        longWord("celular");
        return false;
    }
    if (email.length > 50) {
        longWord("correo electrónico");
        return false;
    }

    if (isNaN(age)) {

        Swal.fire({
            icon: 'error',
            title: 'La edad ingresada no es un número',

        })
        return false;
    }

    if (!checkEmail(email)) {
        Swal.fire({
            icon: 'error',
            title: 'el correo electrónico no es valido',
        })
        return false;
    }

    if (password.length > 50) {
        longWord("contraseña");
        return false;
    }

    if (password != passwordV) {

        Swal.fire({
            icon: 'error',
            title: 'Las contraseñas no coinciden',

        })
        return false;
    }


    saveRegistro();

}

function longWord(area) {
    Swal.fire({
        icon: 'error',
        title: 'La palabra ingresada en el campo ' + area + ' es muy larga',

    })
}

function shortWord(area) {
    Swal.fire({
        icon: 'error',
        title: 'La palabra ingresada en el campo ' + area + ' es muy corta',

    })
}

function checkEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email) ? true : false;
}

function saveRegistro() {
    var datos = $('#formRegistro').serialize();

    $.post("saveRegistro", datos).done(function(data) {
        var variable = 2;
        variable = data;

        if (variable == 1) {

            Swal.fire({
                icon: 'success',
                title: 'Se ha guardado con exito. ',
            })
        } else if (variable == 2) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error, favor de recargar la página.',
            })
        } else if (variable == 3) {
            Swal.fire({
                icon: 'error',
                title: 'El usuario ya existe, ingresa otro.',
            })
        } else if (variable == 4) {
            Swal.fire({
                icon: 'error',
                title: 'El correo ya existe, ingresa otro.',
            })
        }

    });
    return false;
}