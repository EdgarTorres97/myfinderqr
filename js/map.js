$(document).ready(function() {
    menu();
    footer();
});

function menu() {
    $.ajax({
        url: 'menu',
        success: function(respuesta) {
            $('.menu').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function footer() {
    $.ajax({
        url: 'footer',
        success: function(respuesta) {
            $('.footer').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function initMap() {

    map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 21.1713976,
            lng: -86.8712448
        },
        zoom: 13
    });
    $.ajax({
        url: 'getCentros',
        success: function(respuesta) {
            markers = JSON.parse(respuesta);
            for (const prop in markers) {
                //console.log(`markers.${prop} = ${markers[prop]}`);
                // console.log(parseFloat(markers[prop].latitud), parseFloat(markers[prop].longitud))
                addMarker(markers[prop]);
            }
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }

    });

    infoWindow = new google.maps.InfoWindow;



    // The location of Uluru
    // The map, centered at Uluru


}

function addMarker(props) {
    // console.log(props.nombreCentro, props.latitud, props.longitud);
    if (props.status != 0) {
        // console.log(props.nombreCentro, props.latitud, props.longitud, props.status);

        //return;
        var marker = new google.maps.Marker({
            position: {
                lat: parseFloat(props.latitud),
                lng: parseFloat(props.longitud)
            },
            map: map,
            title: props.nombreCentro,

            icon: 'styles/images/centroacopio.png',
            // content: '<a href="/playas/' + props.id + '"> hola' + props.nombre + '</a>',
            //icon:props.iconImage
        });
        google.maps.event.addListener(marker, 'click', function() {
            //alert(props.nombre);
            window.location = 'infoCentro?centro=' + props.idCentro;
            // console.log("hola");
        });
        if (props.nombreCentro) {
            var contentString =
                '<div class="content">' +
                '<h6>' + props.nombreCentro + '<br>' +
                '<br><img src="styles/images/relog.png" alt="">' + props.horario +
                '<br><img src="styles/images/telefono.png" alt="">' + props.numero +
                '<br><a href="' + props.ubicacion + '"><img src="styles/images/mapIcon.png" alt=""></a>Ubicación' +
                '</div>';
            // contentString+=props.edgar==1?' Playa Limpia':' Playa contaminada';
            var infoWindow = new google.maps.InfoWindow({

                content: contentString
            });

            infoWindow.open(map, marker);


            marker.addListener('click', function() {
                infoWindow.open(map, marker);

            });


        }
    }

}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
}

function traffic() {

    var trafficLayer = new google.maps.TrafficLayer();
    trafficLayer.setMap(map);



}

function ubicacion() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            // console.log("entro");


            map.setCenter(pos);
            var marker2 = new google.maps.Marker({
                position: {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                },
                icon: 'styles/images/gps.png',
                content: '<h4>Tu ubicación</h4>'
            });
            var contentString =
                '<div class="content"> Tu ubicación</div>';
            var infoWindow = new google.maps.InfoWindow({

                content: contentString
            });
            marker2.setMap(map);
            infoWindow.open(map, marker2);
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });




    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }
}