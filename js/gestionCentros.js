$(document).ready(function() {
    menu();
    footer();

    buscadorCentros();
    buscadorCentrosBaja();
    formUpdateCentros();

    $(buscar_datos());

    buscar_datos();
    $(buscar_datos_baja());

    buscar_datos_baja();

});

function menu() {
    $.ajax({
        url: 'menu',
        success: function(respuesta) {
            $('.menu').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function footer() {
    $.ajax({
        url: 'footer',
        success: function(respuesta) {
            $('.footer').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function formCentros() {
    ocultar =
        '<h1 class="titulo" id="mostrarFormCentros">Gestión de centros <i class="fas fa-minus-circle" onclick="ocultarFormCentros()" style="color: red;"></i></h1>';
    $.ajax({
        url: 'formCentros',
        success: function(respuesta) {
            $('.formCentros').html(respuesta)
            $('#mostrarFormCentros').html(ocultar);
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function ocultarFormCentros() {
    mostrar =
        '<h1 class="titulo" id="mostrarFormCentros">Gestión de centros <i class="fas fa-plus-circle" onclick="formCentros()" style="color: #007bff;"></i></h1>';
    $('.formCentros').html("");
    $('#mostrarFormCentros').html(mostrar);
}


function buscadorCentros() {
    $.ajax({
        url: 'buscadorCentros',
        success: function(respuesta) {
            $('.buscadorCentros').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function buscadorCentrosBaja() {
    $.ajax({
        url: 'buscadorCentrosBaja',
        success: function(respuesta) {
            $('.buscadorCentrosBaja').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function formUpdateCentros() {
    $.ajax({
        url: 'formUpdateCentros',
        success: function(respuesta) {
            $('.formUpdateCentros').html(respuesta)

            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function ocultarFormUpdateCentros() {
    $('.formUpdateCentros').html("");
}

function buscar_datos(consulta) {
    $.ajax({
            url: 'getTableCentros',
            type: 'POST',
            dataType: 'html',
            data: { consulta: consulta },
        })
        .done(function(respuesta) {
            $('.getTableCentros').html(respuesta);

        })
        .fail(function() {
            console.log("error");
        })
    return false;
}

$(document).on('keyup', '#caja_busqueda_baja', function() {
    var valor = $(this).val();
    if (valor != "") {
        buscar_datos_baja(valor);
    } else {
        buscar_datos_baja();
    }
})

function buscar_datos_baja(consulta) {
    $.ajax({
            url: 'getTableCentrosBaja',
            type: 'POST',
            dataType: 'html',
            data: { consulta: consulta },
        })
        .done(function(respuesta) {
            $('.getTableCentrosBaja').html(respuesta);

        })
        .fail(function() {
            console.log("error");
        })
    return false;
}

$(document).on('keyup', '#caja_busqueda', function() {
    var valor = $(this).val();
    if (valor != "") {
        buscar_datos(valor);
    } else {
        buscar_datos();
    }
})

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 21.1713976,
            lng: -86.8712448
        },
        zoom: 12
    });
    $.ajax({
        url: 'getCentros',
        success: function(respuesta) {
            markers = JSON.parse(respuesta);
            for (const prop in markers) {
                // console.log(`markers.${prop} = ${markers[prop]}`);
                // console.log(parseFloat(markers[prop].latitud), parseFloat(markers[prop].longitud))
                addMarker(markers[prop]);
            }
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }

    });

    // The location of Uluru
    // The map, centered at Uluru
    function addMarker(props) {
        // console.log(props.latitud);
        //return;
        var marker = new google.maps.Marker({
            position: {
                lat: parseFloat(props.latitud),
                lng: parseFloat(props.longitud)
            },
            map: map,
            title: 'Hello World!',

            icon: 'styles/images/palmera.png',
            content: '<p>' + props.nombreCentro + '</p>'
                // content: '<a href="/playas/' + props.id + '"> hola' + props.nombre + '</a>',
                //icon:props.iconImage
        });

    }

}





function saveCentro() {

    nombreCentro = $('#nombreCentro').val();
    numero = $('#numero').val();
    estado = $('#estado').val();
    horario = $('#horario').val();
    ubicacion = $('#ubicacion').val();
    tipo = $('#tipo').val();
    latitud = $('#latitud').val();
    longitud = $('#longitud').val();

    if (nombreCentro === "" || numero === "" || estado === "" || horario === "" || ubicacion === "" || tipo === "" || latitud === "" || longitud === "") {

        Swal.fire({
            icon: 'error',
            title: 'Todos los campos son obligatorios',
        })
        return false;
    }

    if (!isFloat_number(parseFloat(latitud)) && isNaN(latitud)) {


        Swal.fire({
            icon: 'error',
            title: 'La latitud debe ser un número',

        })
        return false;


    }
    if (!isFloat_number(parseFloat(longitud)) && isNaN(longitud)) {

        Swal.fire({
            icon: 'error',
            title: 'La longitud debe ser un número',

        })
        return false;


    }


    if (nombreCentro.length > 50) {
        longWord("nombre del centro");
        return false;
    }
    if (numero.length > 50) {
        longWord("número de teléfono o celular");
        return false;
    }
    if (estado.length > 50) {
        longWord("estado de la república");
        return false;
    }
    if (horario.length > 50) {
        longWord("horario");
        return false;
    }
    if (ubicacion.length > 300) {
        longWord("ubicacion");
        return false;
    }
    if (tipo.length > 50) {
        longWord("tipo de centro");
        return false;
    }
    if (latitud.length > 18) {
        longWord("latitud");
        return false;
    }
    if (longitud.length > 18) {
        longWord("longitud");
        return false;
    }

    function longWord(area) {
        Swal.fire({
            icon: 'error',
            title: 'La palabra ingresada en el campo ' + area + ' es muy larga',

        })
    }
    var myForm = $("#formCentro")[0]

    // var datos = $('form#Centro').serialize();

    // var formData = new FormData($('#formCentro'));

    $.ajax({
        url: 'saveCentro',
        type: 'POST',
        data: new FormData(myForm),
        success: function(variable) {

            if (variable == 1) {

                Swal.fire({
                    icon: 'success',
                    title: 'Se ha guardado con exito. ',
                })
            } else if (variable == 2) {
                Swal.fire({
                    icon: 'error',
                    title: 'Ha ocurrido un error, favor de recargar la página.',
                })
            } else if (variable == 3) {
                Swal.fire({
                    icon: 'error',
                    title: 'El centro ya existe, ingresa otro.',
                })
            }
        },
        cache: false,
        contentType: false,
        processData: false
    });
    /*
    $.post("saveCentro", datos).done(function(data) {
        var variable = 2;
        variable = data;

        if (variable == 1) {

            Swal.fire({
                icon: 'success',
                title: 'Se ha guardado con exito. ',
            })
        } else if (variable == 2) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error, favor de recargar la página.',
            })
        } else if (variable == 3) {
            Swal.fire({
                icon: 'error',
                title: 'El centro ya existe, ingresa otro.',
            })
        }

    });*/
    return false;
}

function isFloat_number(v) {
    var num = /^[-+]?[0-9]+\.[0-9]+$/;
    return num.test(v);
}

function modificarCentro(id) {
    $.ajax({
            url: 'formUpdateCentros',
            type: 'POST',
            dataType: 'html',
            data: { idCentro: id },
        })
        .done(function(respuesta) {
            $('.formUpdateCentros').html(respuesta);

        })
        .fail(function() {
            console.log("error");
        })
    return false;
}

function updateCentro() {
    var datos = $('#formUpdateCentro').serialize();

    $.post("updateCentro", datos).done(function(data) {
        var variable = 2;
        variable = data;
        if (variable == 1) {
            Swal.fire({
                icon: 'success',
                title: 'Se ha guardado con exito. ',
            })
        } else {
            Swal.fire({
                icon: 'error',
                title: 'No se ha podido actualizar. ',
            })
        }
        buscar_datos();

    });
    return false;
}

function deleteCentro(idCentro) {
    console.log("hola");
    console.log(idCentro);
    $.ajax({
            url: 'deleteCentro',
            type: 'POST',
            dataType: 'html',
            data: { idCentro: idCentro },
        })
        .done(function(respuesta) {
            if (respuesta == 1) {
                Swal.fire({
                    icon: 'success',
                    title: 'Se ha guardado con exito. ',
                })
                $('.formUpdateCentros').html("");
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Ha ocurrido un error. ',
                })
            }
            buscar_datos();
        })
        .fail(function() {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error. ',
            })
        })
}



function deleteLogicoCentro(idCentro, nombreCentro) {
    Swal.fire({
        title: 'Esta seguro de dar de baja el centro: ' + nombreCentro,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {

            $.ajax({
                    url: 'deleteLogicoCentro',
                    type: 'POST',
                    dataType: 'html',
                    data: { idCentro: idCentro },
                })
                .done(function(respuesta) {
                    if (respuesta == 1) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Se ha eliminado con exito. ',
                        })
                        $('.formUpdateUsuarios').html("");
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Ha ocurrido un error. ',
                        })
                    }
                    buscar_datos();
                    buscar_datos_baja();
                })
                .fail(function() {
                    Swal.fire({
                        icon: 'error',
                        title: 'Ha ocurrido un error. ',
                    })
                })

            return false;
        }
    });


}

function createLogicoCentro(idCentro, nombreCentro) {
    Swal.fire({
        title: 'Esta seguro de dar de alta el centro: ' + nombreCentro,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {

            $.ajax({
                    url: 'createLogicoCentro',
                    type: 'POST',
                    dataType: 'html',
                    data: { idCentro: idCentro },
                })
                .done(function(respuesta) {
                    if (respuesta == 1) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Se ha eliminado con exito. ',
                        })
                        $('.formUpdateUsuarios').html("");
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Ha ocurrido un error. ',
                        })
                    }
                    buscar_datos();
                    buscar_datos_baja();
                })
                .fail(function() {
                    Swal.fire({
                        icon: 'error',
                        title: 'Ha ocurrido un error. ',
                    })
                })

            return false;
        }
    });


}