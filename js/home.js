$(document).ready(function() {

    $.ajax({
        url: 'menu',
        success: function(respuesta) {
            $('.menu').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
    $.ajax({
        url: 'footer',
        success: function(respuesta) {
            $('.footer').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
    $.ajax({
        url: 'formulario',
        success: function(respuesta) {
            $('.formulario').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });

    $.ajax({
        url: 'formularioTarjeta',
        success: function(respuesta) {
            $('.formularioTarjeta').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });

    actualizarMostrarTarjetas();
    var interval = setInterval(actualizarMostrarTarjetas, 50000);
});

function enviar() {
    var datos = $('#formEnviar').serialize();

    $.post("enviar", datos).done(function(data) {
        var variable = 2;
        variable = data;

        if (variable == 1) {

            Swal.fire({
                icon: 'success',
                title: 'Se ha guardado con exito. ',
            })
        } else if (variable == 2) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error, favor de recargar la página.',
            })
        }

    });
    return false;
}

function guardarTarjeta() {
    var datos = $('#formTarjeta').serialize();

    $.post("guardarTarjeta", datos).done(function(data) {
        var variable = 2;
        variable = data;

        if (variable == 1) {

            Swal.fire({
                icon: 'success',
                title: 'Se ha guardado con exito. ',
            })
        } else if (variable == 2) {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error, favor de recargar la página.',
            })
        }

    });
    return false;
}

function actualizarMostrarTarjetas() {
    $.ajax({
        url: 'tarjetas',
        success: function(respuesta) {
            $('.tarjetas').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function actualizarTarjeta() {
    var datos = $('#formModTarjeta').serialize();

    $.post("actualizarTarjeta", datos).done(function(data) {
        var variable = 2;
        variable = data;
        if (variable == 1) {
            Swal.fire({
                icon: 'success',
                title: 'Se ha guardado con exito. ',
            })
        } else {
            Swal.fire({
                icon: 'error',
                title: 'No se ha podido actualizar. ',
            })
        }


    });
    return false;
}

function eliminarTarjeta(idTar) {
    console.log("hola");
    console.log(idTar);
    $.ajax({
            url: 'eliminarTarjeta',
            type: 'POST',
            dataType: 'html',
            data: { idTarjeta: idTar },
        })
        .done(function(respuesta) {
            if (respuesta == 1) {
                Swal.fire({
                    icon: 'success',
                    title: 'Se ha guardado con exito. ',
                })
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Ha ocurrido un error. ',
                })
            }

        })
        .fail(function() {
            Swal.fire({
                icon: 'error',
                title: 'Ha ocurrido un error. ',
            })
        })
}

function ver(nombre, numTarjeta, tipo, mes, anio, idTar) {
    Swal.fire({
        title: 'Nombre: ' + nombre,
        text: 'Número de tarjeta: ' + numTarjeta + 'Tipo: ' + tipo + mes + anio + idTar,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Modificar'
    }).then((result) => {
        if (result.value) {

            $.ajax({
                    url: 'modificarTarjeta',
                    type: 'POST',
                    dataType: 'html',
                    data: { idTarjeta: idTar },
                })
                .done(function(respuesta) {
                    $('.modificar').html(respuesta)
                })
                .fail(function() {
                    console.log("error");
                })

            return false;
        }
    });


}

function crearTabla() {
    var datos = $('#formCrearTabla').serialize();

    $.post("crearTabla", datos).done(function(data) {
        var variable = 2;
        variable = data;
        console.log(data);

    });
    return false;
}