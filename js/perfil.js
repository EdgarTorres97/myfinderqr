$(document).ready(function() {
    menu()
    formUpdatePerfil();
    formUpdatePassword();
    footer()
});

function formUpdatePerfil() {
    $.ajax({
        url: 'formUpdatePerfil',
        success: function(respuesta) {
            $('.formUpdatePerfil').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function formUpdatePassword() {
    $.ajax({
        url: 'formUpdatePassword',
        success: function(respuesta) {
            $('.formUpdatePassword').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}


function modificarPerfil() {
    Swal.fire({
        title: '¿Está seguro de guardar los cambios? ',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.value) {

            var datos = $('#formModificarPerfil').serialize();

            $.post("updatePerfilUsuario", datos).done(function(data) {
                var variable = 2;
                variable = data;
                if (variable == 1) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Se ha guardado con exito. ',
                    })
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'No se ha podido actualizar. ',
                    })
                }

            });
            return false;


        }
    });


}

function modificarPassword() {

    clave = document.getElementById("claven").value;
    claveV = document.getElementById("rclaven").value;
    if (clave === "" || claveV === "") {
        Swal.fire({
            icon: 'error',
            title: 'Todos los campos son obligatorios. ',
        });
        return false;
    }
    if (claveV != clave) {

        Swal.fire({
            icon: 'error',
            title: 'Las contraseñas no coinciden. ',
        })
        return false;
    } else {
        Swal.fire({
            title: '¿Está seguro de cambiar la contraseña? ',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {

                var datos = $('#formModificarPassword').serialize();

                $.post("updatePasswordUsuario", datos).done(function(data) {
                    var variable = 2;
                    variable = data;
                    if (variable == 1) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Se ha guardado con exito. ',
                        })
                    } else if (variable == 5) {
                        Swal.fire({
                            icon: 'error',
                            title: 'La clave antigua ingresada es incorrecta. ',
                        })
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'No se ha podido actualizar. ',
                        })
                    }

                });
                return false;


            }
        });
    }

}

function menu() {
    $.ajax({
        url: 'menu',
        success: function(respuesta) {
            $('.menu').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function footer() {
    $.ajax({
        url: 'footer',
        success: function(respuesta) {
            $('.footer').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}