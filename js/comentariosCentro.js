$(document).ready(function() {
    menu();
    footer();
    idCentro = $('#idCentro').val();
    // console.log(idCentro);
    cardCentro(idCentro);
    comentarios(idCentro);
});

function cardCentro(idCen) {

    $.ajax({
            url: 'cardCentro',
            type: 'POST',
            dataType: 'html',
            data: { idCentro: idCen },
        })
        .done(function(respuesta) {
            //console.log(respuesta);
            $('.cardComentarios').html(respuesta);
            //$('.getTableCentrosBaja').html(respuesta);

        })
        .fail(function() {
            console.log("error");

        })
    return false;
}

function comentarios(idCen) {
    idCentro = $('#idCentro').val();
    $.ajax({
            url: 'comentariosCentro',
            type: 'POST',
            dataType: 'html',
            data: { idCentro: idCen },
        })
        .done(function(respuesta) {
            $('.comentarios').html(respuesta);

        })
        .fail(function() {
            console.log("error");

        })
    return false;
}

function likesComentarios(idComen) {
    //console.log("hola");
    id = '#like' + idComen;
    $.ajax({
            url: 'likesComentariosCentro',
            type: 'POST',
            dataType: 'html',
            data: { idComentario: idComen },
        })
        .done(function(respuesta) {
            // console.log(respuesta);
            //console.log(id);
            $(id).html(respuesta);
            //$('.getTableCentrosBaja').html(respuesta);

        })
        .fail(function() {
            console.log("error");

        })
    return false;
}

function dislikesComentarios(idComen) {
    //console.log("hola");
    id = '#dislike' + idComen;
    $.ajax({
            url: 'dislikesComentariosCentro',
            type: 'POST',
            dataType: 'html',
            data: { idComentario: idComen },
        })
        .done(function(respuesta) {
            // console.log(respuesta);
            //console.log(id);
            $(id).html(respuesta);
            //$('.getTableCentrosBaja').html(respuesta);

        })
        .fail(function() {
            console.log("error");

        })
    return false;
}

function menu() {
    $.ajax({
        url: 'menu',
        success: function(respuesta) {
            $('.menu').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function footer() {
    $.ajax({
        url: 'footer',
        success: function(respuesta) {
            $('.footer').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function formComentarios() {
    ocultar =
        '<h3 id="agregarComentario">Agregar comentario<i class="fas fa-minus-circle" onclick="ocultarFormComentarios()" style="color: red;"></i></h3>';
    $.ajax({
        url: 'formComentariosCentro',
        success: function(respuesta) {
            $('.formComentariosCentro').html(respuesta)
            $('#agregarComentario').html(ocultar);
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function ocultarFormComentarios() {
    mostrar =
        '<h3 id="agregarComentario">Agregar comentario<i class="fas fa-plus-circle" onclick="formComentarios()" style="color: #007bff;"></i></h3>';
    $('.formComentariosCentro').html("");
    $('#agregarComentario').html(mostrar);
}


function like(idComen) {
    idCentro = $('#idCentro').val();
    $.ajax({
            url: 'agregarLike',
            type: 'POST',
            dataType: 'html',
            data: { idComentario: idComen },
        })
        .done(function(respuesta) {
            //console.log(respuesta);
            likesComentarios(idComen);
            //$('.getTableCentrosBaja').html(respuesta);

        })
        .fail(function() {
            console.log("error");
        })
    return false;
}

function dislike(idComen) {
    idCentro = $('#idCentro').val();
    $.ajax({
            url: 'agregardisLike',
            type: 'POST',
            dataType: 'html',
            data: { idComentario: idComen },
        })
        .done(function(respuesta) {
            //console.log(respuesta);
            dislikesComentarios(idComen);
            //$('.getTableCentrosBaja').html(respuesta);

        })
        .fail(function() {
            console.log("error");
        })
    return false;
}

function saveComentarioCentro() {
    console.log("entro");
    titulo = $('#titulo').val();
    comentario = $('#comentario').val();
    //idCentro

    if (titulo === "" || comentario === "") {

        Swal.fire({
            icon: 'error',
            title: 'Todos los campos son obligatorios',
        })
        return false;
    }

    if (titulo.length > 45) {
        longWord("titulo");
        return false;
    }
    if (comentario.length > 280) {
        longWord("comentario");
        return false;
    }

    function longWord(area) {
        Swal.fire({
            icon: 'error',
            title: 'La palabra ingresada en el campo ' + area + ' es muy larga',

        })
    }
    mostrar =
        '<h3 id="agregarComentario">Agregar comentario<i class="fas fa-plus-circle" onclick="formComentarios()" style="color: #007bff;"></i></h3>';

    idCentro = $('#idCentro').val();
    $.ajax({
            url: 'guardarComentariosCentro',
            type: 'POST',
            dataType: 'html',
            data: { idCentro: idCentro, titulo: titulo, comentario: comentario },
        })
        .done(function(respuesta) {

            if (respuesta == 1) {
                Swal.fire({
                    icon: 'success',
                    title: 'Se ha guardado el comentario con exito!',
                })
                $('.formComentariosCentro').html("");
                $('#agregarComentario').html(mostrar);
                comentarios(idCentro);
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Ha ocurrido un error',

                })
            }

        })
        .fail(function() {
            console.log("error");
        })
    return false;

}