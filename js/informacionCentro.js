$(document).ready(function() {
    menu();
    footer();
    informacionCentro();
});

function menu() {
    $.ajax({
        url: 'menu',
        success: function(respuesta) {
            $('.menu').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function footer() {
    $.ajax({
        url: 'footer',
        success: function(respuesta) {
            $('.footer').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}

function informacionCentro() {
    $.ajax({
        url: 'getInfoCentro',
        success: function(respuesta) {
            $('.getInfoCentro').html(respuesta)
            return false;
        },
        error: function() {
            console.log("No se ha podido obtener la información");
            return false;
        }
    });
}