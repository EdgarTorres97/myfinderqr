<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script>
    <script src="js/perfil.js"></script>

    <script defer src="fontawesome/solid.js"></script>
    <script defer src="fontawesome/fontawesome.js"></script>
    <link rel="stylesheet" href="styles/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles/css/view.css">
    <link rel="stylesheet" href="styles/css/perfil.css">

    <title><?php echo $_SESSION['userName']; ?></title>
</head>

<body>
    <div class="menu"></div>


    <div id="contenido">

        <img id="imgPerfil" src="styles/images/img_avatar.png" alt="Avatar">
        <div class="formUpdatePerfil"></div>
        <div class="formUpdatePassword"></div>


    </div>
    <div class="footer"></div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>