<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script>
    <script src="js/map.js?v1"></script>

    <script defer src="fontawesome/solid.js"></script>
    <script defer src="fontawesome/fontawesome.js"></script>
    <link rel="stylesheet" href="styles/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles/css/view.css">
    <link rel="stylesheet" href="styles/css/mapa.css">

    <title>Mapa</title>
</head>

<body>
    <div class="menu"></div>

    <div class="contenedor">
        <h3>Centros de acopio y ayuda sobre el covid-19</h3>

        <div id="map"></div>

        <img id="imgTrafico" src="styles/images/trafico.png" alt="Trafico" onclick="traffic()">
        <img id="imgUbicacion" src="styles/images/mapMarker.png" alt="Tu ubicación" onclick="ubicacion()">

    </div>



    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDhrUeKD7rOyi7Zfvk6B3B1JQhNB4Au3Y&callback=initMap">
    </script>

    <div class="footer"></div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>