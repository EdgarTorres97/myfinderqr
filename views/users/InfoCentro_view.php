<?php
$centro = $this->centro;
$comentario = $this->comentario;

?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script>
    <script src="js/informacionCentro.js"></script>
    <script src="js/comentariosCentro.js"></script>

    <script defer src="fontawesome/solid.js"></script>
    <script defer src="fontawesome/fontawesome.js"></script>
    <link rel="stylesheet" href="styles/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles/css/view.css">
    <link rel="stylesheet" href="styles/css/informacionCentro.css">

    <title><?php echo $centro['nombreCentro']; ?></title>
</head>

<body>
    <div class="menu"></div>

    <div id="contenido">
        <h3><?php echo $centro['nombreCentro']; ?></h3>
        <div class="cardComentarios">

        </div>
        <input class="form-control" type="hidden" id="idCentro" name="idCentro" value="<?php echo $centro['idCentro']; ?>">

        <div class="comentariosContenido">
            <h3 id="agregarComentario">Agregar comentario<i class="fas fa-plus-circle" onclick="formComentarios()" style="color: #007bff;"></i></h3>

            <div class="formComentariosCentro">

            </div>

            <div class="comentarios">

            </div>
        </div>


        <?php



        ?>

    </div>

    <div class="footer"></div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>