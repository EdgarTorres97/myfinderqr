<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script>
    <script src="js/gestionCentros.js"></script>

    <script defer src="fontawesome/solid.js"></script>
    <script defer src="fontawesome/fontawesome.js"></script>
    <link rel="stylesheet" href="styles/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles/css/view.css">
    <link rel="stylesheet" href="styles/css/centro.css">


    <title>Gestión centros</title>
</head>

<body>
    <div class="menu"></div>



    <div id="contenido">

        <h1 class="titulo" id="mostrarFormCentros">Gestión de centros
            <i class="fas fa-plus-circle" onclick="formCentros()" style="color: #007bff;"></i>

        </h1>


        <div class="formCentros"></div>

        <div class="buscadorCentros"></div>

        <div class="getTableCentros"></div>

        <div class="buscadorCentrosBaja"></div>
        
        <div class="getTableCentrosBaja"></div>

        <div class="formUpdateCentros"></div>
    </div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>