<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/sweetalert2.all.min.js"></script>
    <script src="js/login.js?v1"></script>

    <script defer src="fontawesome/solid.js"></script>
    <script defer src="fontawesome/fontawesome.js"></script>
    <link rel="stylesheet" href="styles/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles/css/view.css">
    <link rel="stylesheet" href="styles/css/login.css">

    <link rel="stylesheet" href="styles/css/formulario.css">

    <title>Login</title>
</head>

<body>
    <div class="menu"></div>


    <div id="contenido">
        <div class="login"></div>

    </div>

    <div class="footer"></div>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>

</html>