
<?php

define("ROOT_PATH", __DIR__);
//echo __DIR__;
require_once ROOT_PATH . '/controller/pageController.php';
require_once ROOT_PATH . '/controller/userController.php';
require_once ROOT_PATH . '/controller/adminController.php';
require_once ROOT_PATH . '/controller/conocenosController.php';
require_once ROOT_PATH . '/controller/gestionCentroController.php';
require_once ROOT_PATH . '/controller/gestionUsuarioController.php';
require_once ROOT_PATH . '/controller/comentariosCentrosController.php';

//definimos la variable para usar el controlador
$pageController = new pageController();
$userController = new userController();
$adminController = new adminController();
$conocenosController = new conocenosController();
$gestionCentroController = new gestionCentroController();
$gestionUsuarioController = new gestionUsuarioController();
$comentariosCentrosController = new comentariosCentrosController();

session_start();
// echo $_SESSION['rolId'];
// echo $_SESSION['rolId'];


if (isset($_GET['url'])) {
    $pagina = $_GET['url'];


    if ($pagina == 'menu') {
        echo $pageController->menu();
    } else if ($pagina == 'footer') {
        echo $pageController->footer();
    } else if (!isset($_SESSION['rolId'])) {
        if ($pagina == 'login') {
            echo $userController->loginView();
        } else if ($pagina == 'registro') {
            echo $userController->registerView();
        } else if ($pagina == 'formRegistro') {
            echo $userController->formRegistro();
        } else if ($pagina == 'saveRegistro') {
            echo $userController->saveRegistro();
        } else if ($pagina == 'formLogin') {
            echo $userController->formLogin();
        } else if ($pagina == 'valida') {
            echo $userController->valida();
        } else {
            echo $userController->loginView();
        }
    } else if ($_SESSION['rolId'] == 1 || $_SESSION['rolId'] == 2) {
        if ($pagina == 'endSesion') {
            echo $userController->endSesion();
        } else if ($pagina == 'centros') {
            echo $gestionCentroController->mapView();
        } else if ($pagina == 'getCentros') {
            echo $gestionCentroController->getCentros();
        } else if ($pagina == 'perfil') {
            echo $userController->perfil();
        } else if ($pagina == 'formUpdatePerfil') {
            echo $userController->formUpdatePerfil();
        } else if ($pagina == 'updatePerfilUsuario') {
            echo $userController->updatePerfilUsuario();
        } else if ($pagina == 'formUpdatePassword') {
            echo $userController->formUpdatePassword();
        } else if ($pagina == 'updatePasswordUsuario') {
            echo $userController->updatePasswordUsuario();
        } else if ($pagina == 'informacionCentro') {
            echo $gestionCentroController->informacionCentroView();
        } else if ($pagina == 'getInfoCentro') {
            echo $gestionCentroController->getInfoCentros();
        }else if ($pagina == 'infoCentro') {
            echo $gestionCentroController->infoCentroView();
        }else if ($pagina == 'formComentariosCentro') {
            echo $comentariosCentrosController->formularioComentariosCentros();
        }else if ($pagina == 'cardCentro') {
            echo $comentariosCentrosController->cardInfoCentor();
        }else if ($pagina == 'guardarComentariosCentro') {
            echo $comentariosCentrosController->guardarComentarioCentro();
        }else if ($pagina == 'comentariosCentro') {
            echo $comentariosCentrosController->comentarios();
        }else if ($pagina == 'likesComentariosCentro') {
            echo $comentariosCentrosController->likesComentario();
        }else if ($pagina == 'agregarLike') {
            echo $comentariosCentrosController->agregarLike();
        }else if ($pagina == 'dislikesComentariosCentro') {
            echo $comentariosCentrosController->dislikesComentario();
        }else if ($pagina == 'agregardisLike') {
            echo $comentariosCentrosController->agregardisLike();
        }else if ($_SESSION['rolId'] == 1) {
            if ($pagina == 'admin') {
                echo $adminController->adminView();
            } else if ($pagina == 'gestionCentros') {
                echo $gestionCentroController->centrosView();
            } else if ($pagina == 'formCentros') {
                echo $gestionCentroController->formularioCentros();
            } else if ($pagina == 'saveCentro') {
                echo $gestionCentroController->saveCentro();
            } else if ($pagina == 'buscadorCentros') {
                echo $gestionCentroController->buscadorCentros();
            } else if ($pagina == 'getTableCentros') {
                echo $gestionCentroController->getTableCentros();
            } else if ($pagina == 'buscadorCentrosBaja') {
                echo $gestionCentroController->buscadorCentrosBaja();
            } else if ($pagina == 'getTableCentrosBaja') {
                echo $gestionCentroController->getTableCentrosBaja();
            }else if ($pagina == 'formUpdateCentros') {
                echo $gestionCentroController->formularioUpdateCentros();
            } else if ($pagina == 'updateCentro') {
                echo $gestionCentroController->updateCentro();
            } else if ($pagina == 'deleteCentro') {
                echo $gestionCentroController->deleteCentro();
            } else if ($pagina == 'deleteLogicoCentro') {
                echo $gestionCentroController->deleteLogicoCentro();
            } else if ($pagina == 'createLogicoCentro') {
                echo $gestionCentroController->createLogicoCentro();
            } else if ($pagina == 'gestionUsuarios') {
                echo $gestionUsuarioController->gestionUsuariosView();
            } else if ($pagina == 'formUsuarios') {
                echo $gestionUsuarioController->formularioUsuarios();
            } else if ($pagina == 'saveUsuario') {
                echo $gestionUsuarioController->saveUsuario();
            } else if ($pagina == 'buscadorUsuarios') {
                echo $gestionUsuarioController->buscadorUsuarios();
            } else if ($pagina == 'getTableUsuarios') {
                echo $gestionUsuarioController->getTableUsuarios();
            } else if ($pagina == 'buscadorUsuariosBaja') {
                echo $gestionUsuarioController->buscadorUsuariosBaja();
            } else if ($pagina == 'getTableUsuariosBaja') {
                echo $gestionUsuarioController->getTableUsuariosBaja();
            } else if ($pagina == 'formUpdateUsuarios') {
                echo $gestionUsuarioController->formularioUpdateUsuarios();
            } else if ($pagina == 'updateUsuario') {
                echo $gestionUsuarioController->updateUsuario();
            } else if ($pagina == 'cambiarClaveUsuario') {
                echo $gestionUsuarioController->cambiarClave();
            } else if ($pagina == 'deleteLogicoUsuario') {
                echo $gestionUsuarioController->deleteLogicoUsuario();
            } else if ($pagina == 'createLogicoUsuario') {
                echo $gestionUsuarioController->createLogicoUsuario();
            } else {
                header('location: centros');
            }
        } else {
            header('location: centros');
        }
    } else {
        header('location: login');
    }
} else {
    header('location: login');
}
