<?php
require_once ROOT_PATH . '/libs/Controller.php';
require_once ROOT_PATH . '/libs/View.php';
require_once ROOT_PATH . '/model/Home.php';
require_once ROOT_PATH . '/model/Tarjeta.php';

class adminController extends Controller
{

    public function adminView()
    {
        return new View('admin/admin_view');
    }

    public function menu()
    {
        $menu = 
        '
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="login">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="endSesion">cerrar sesión</a>
                    </li>
                </ul>
            </div>
        </nav>
        ';
        return $menu;
    }

    

    public function enviar()
    {
        $formulario = 
        '
        <form id="formEnviar" method="POST">
        <input type="text" name="description">
        <input type="text" name="nombre">
        <input  id="btnEnviar" onclick="enviar()" class="btn btn-secondary" type="button" value="Enviar">
        </form>
        ';
        return $formulario;
    }

    
}
