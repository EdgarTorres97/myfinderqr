<?php
require_once ROOT_PATH . '/libs/Controller.php';
require_once ROOT_PATH . '/libs/View.php';
require_once ROOT_PATH . '/model/Centro.php';
require_once ROOT_PATH . '/model/Tarjeta.php';

class pageController extends Controller
{

    public function menu()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $menu =
                '
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">Navbar</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        
                        ';
            if (!isset($_SESSION['rolId'])) {
                $menu .=
                    '
                            <li class="nav-item active">
                                <a class="nav-link" href="registro">Registro <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link" href="login">Login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">contacto</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Conocenos</a>
                            </li>
                            ';
            } else if ($_SESSION['rolId'] == 1 || $_SESSION['rolId'] == 2) {
                $menu .=
                    '
                            
                            <li class="nav-item">
                            <a class="nav-link" href="centros">Centros</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="informacionCentro">Información Centros</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">información</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">noticias</a>
                        </li>
                            ';
                if ($_SESSION['rolId'] == 1) {
                    $menu .=
                        '
                                
                                <li class="nav-item dropdown" ">
                                <a class="nav-link dropdown-toggle"  href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Estadisticas
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="background-color: #007bff; color: white;">
                                <a class="nav-link" href="#">Estadisticas covid</a>
                                <a class="nav-link" href="#">estadisticas centro</a>
                                
                            </li>
                                <li class="nav-item dropdown" ">
                                    <a class="nav-link dropdown-toggle"  href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Gestión
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="background-color: #007bff; color: white;">
                                    <a class="nav-link" href="gestionUsuarios">Gestion de usuarios</a>
                                    <a class="nav-link" href="gestionCentros">Gestión de centros</a>
                                    
                                </li>
                                ';
                }
                $menu .=
                    '
                    <li class="nav-item dropdown" ">
                        <a class="nav-link dropdown-toggle"  href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Perfil
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="background-color: #007bff; color: white;">
                        <a class="nav-link" href="perfil">perfil</a>
                        <a class="nav-link" href="endSesion">cerrar sesión</a>
                        
                    </li>
                            ';
            }



            $menu .=
                '
                        
                        
                        
                    </ul>
                </div>
            </nav>
            ';

            return $menu;
        } else {
            header('location: login');
        }
    }

    public function footer()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $footer =
                '
                <footer>
                <p>
                INFOVID-19 V.1 <br>
                Desarrollador: Edgar Antonio Torres Hernández.
    
                </p>
            </footer>
            
                        ';
            

        
        
        
        return $footer;
        }else {
            header('location: login');
        }
    }

    

    public function enviar()
    {
        $formulario = '<form id="formEnviar" method="POST">
        <input type="text" name="description">
        <input type="text" name="nombre">
        <input  id="btnEnviar" onclick="enviar()" class="btn btn-secondary" type="button" value="Enviar">
        </form>';
        return $formulario;
    }

    public function saveTarjeta()
    {

        if (
            !empty($_POST["nombre"]) ||
            !empty($_POST["numTarjeta"]) ||
            !empty($_POST["tipo"]) ||
            !empty($_POST["mesTarjeta"]) ||
            !empty($_POST["anioTarjeta"]) ||
            !empty($_POST["codigoSeg"])
        ) {
            $nombre = $_POST['nombre'];
            $numTarjeta = $_POST['numTarjeta'];
            $tipo = $_POST['tipo'];
            $mesTarjeta = $_POST['mesTarjeta'];
            $anioTarjeta = $_POST['anioTarjeta'];
            $codigoSeg = $_POST['codigoSeg'];
            $tarjetas = new Tarjeta();
            $tarjetas->nombreUsuario = addslashes($nombre);
            $tarjetas->numeroTarjeta = addslashes($numTarjeta);
            $tarjetas->tipo = addslashes($tipo);
            $tarjetas->mesVencimiento = addslashes($mesTarjeta);
            $tarjetas->anioVencimiento = addslashes($anioTarjeta);
            $tarjetas->codigoSeguridad = addslashes($codigoSeg);
            $tarjetas->fechaAlta = date("Ymd");
            echo $tarjetas->save();
        } else {
            header('Location: centros');
        }
    }
    public function formularioTarjetas()
    {

        $formularioTarjeta =
            '
            <div id="contenedorFormTarjeta">
            <h5 id="tituloFormTarjeta">Agregar Tarjeta
            <button type="button" id="btnformularioTarjetas" onclick="guardarTarjeta()" style="width: 40px; height: 40px;" class="navbar-btn">
                <i class="fas fa-save"></i>
            </button>
            </h5>
            
            <form id="formTarjeta" method="POST">
                <label for="nombre">Ingrese su nombre:</label>
                <input class="form-control" type="text" name="nombre" placeholder="Ingrese el nombre completo">
                <label for="numTarjeta">Ingrese el número de tarjeta:</label>
                <input class="form-control" type="text" name="numTarjeta" placeholder="Ingrese el numero de la tarjeta">
                <label for="tipo">Ingrese el tipo de tarjeta:</label>
                <input class="form-control" type="text" name="tipo" placeholder="Ingrese el tipo de tarjeta">
                <label for="mesTarjeta">Ingrese el mes de vencimiento:</label>
                <input class="form-control" type="text" name="mesTarjeta" placeholder="Ingrese el mes de vencimiento">
                <label for="anioTarjeta">Ingrese el Año de vencimiento:</label>
                <input class="form-control" type="text" name="anioTarjeta" placeholder="Ingrese el anio de vencimiento">
                <label for="codigoSeg">Ingrese el código de seguridad:</label>
                <input class="form-control" type="text" name="codigoSeg" placeholder="Ingrese el codigo de seguridad">
                
            </form>
            </div>
            ';
        return $formularioTarjeta;
    }

    public function mostrarTarjetas()
    {
        // $tarjetas = Tarjeta::consultaJSON();
        $tarjetadisenio = "";
        $tarjetas = Tarjeta::all();
        $contador = 0;
        $tarjetadisenio .=
            '
            <div id="carouselTarjetas">
            <h2>Tarjetas</h2>
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        
            <div class="carousel-inner">
        ';
        foreach ($tarjetas as $recorrido) {
            if ($contador == 0) {

                $tarjetadisenio .=
                    '
                
                <div class="carousel-item active">
                    <div class="card">';

                if ($recorrido->tipo == 'visa') {
                    $tarjetadisenio .=
                        <<<EOD
                            <div class="card-body" style="background-color: #80ebf3;" onclick="ver('$recorrido->nombreUsuario','$recorrido->numeroTarjeta','$recorrido->tipo','$recorrido->mesVencimiento','$recorrido->anioVencimiento','$recorrido->idTarjeta')">
                            EOD;
                } else {
                    $tarjetadisenio .=
                        <<<EOD
                            <div class="card-body" style="background-color: #bd80f3;" onclick="ver('$recorrido->nombreUsuario','$recorrido->numeroTarjeta','$recorrido->tipo','$recorrido->mesVencimiento','$recorrido->anioVencimiento','$recorrido->idTarjeta')">
                            EOD;
                }
                $tarjetadisenio .=
                    '
                            <h5 class="card-title" id="nombre">' . $recorrido->nombreUsuario . '</h5>
                            <h6 class="card-subtitle mb-2 text-muted" id="numeroTarjeta">' . $recorrido->numeroTarjeta . '</h6>
                            <p class="card-text" id="tipo">' . $recorrido->tipo . '</p>
                            <p class="card-text" id="Vencimiento">' . $recorrido->mesVencimiento . '/' . $recorrido->anioVencimiento . '</p>
                            <input type="hidden" id="idTarjeta" value="' . $recorrido->idTarjeta . '">
                        </div>
                    </div>
                </div>
                ';
                $contador++;
            } else {
                $tarjetadisenio .=
                    '
                <div class="carousel-item">
                    <div class="card">
                    ';
                if ($recorrido->tipo == 'visa') {
                    $tarjetadisenio .=
                        <<<EOD
                            <div class="card-body" style="background-color: #80ebf3;" onclick="ver('$recorrido->nombreUsuario','$recorrido->numeroTarjeta','$recorrido->tipo','$recorrido->mesVencimiento','$recorrido->anioVencimiento','$recorrido->idTarjeta')">
                            EOD;
                } else {
                    $tarjetadisenio .=
                        <<<EOD
                            <div class="card-body" style="background-color: #bd80f3;" onclick="ver('$recorrido->nombreUsuario','$recorrido->numeroTarjeta','$recorrido->tipo','$recorrido->mesVencimiento','$recorrido->anioVencimiento','$recorrido->idTarjeta')">
                            EOD;
                }
                $tarjetadisenio .=
                    '
                        
    
                            <h5 class="card-title" id="nombre">' . $recorrido->nombreUsuario . '</h5>
                            <h6 class="card-subtitle mb-2 text-muted" id="numeroTarjeta">' . $recorrido->numeroTarjeta . '</h6>
                            <p class="card-text" id="tipo">' . $recorrido->tipo . '</p>
                            <p class="card-text" id="Vencimiento">' . $recorrido->mesVencimiento . '/' . $recorrido->anioVencimiento . '</p>
                            <input type="hidden" id="idTarjeta" value="' . $recorrido->idTarjeta . '">

                        </div>
                    </div>
                </div>
                ';
            }
        }
        $tarjetadisenio .=
            '
        
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        </div>
        ';
        return $tarjetadisenio;
    }

    public function modificarTarjeta()
    {
        if (
            !empty($_POST["idTarjeta"])
        ) {
            $idTarjeta = addslashes($_POST["idTarjeta"]);
            $formularioModificarTarjeta = '';
            $tarjetas = Tarjeta::id($idTarjeta);
            if ($tarjetas == 1) {
                echo 1;
            } else {
                $idTarj = $tarjetas['idTarjeta'];
                $formularioModificarTarjeta =
                    '
                    <div id="contenedorFormModTarjeta">
                    <h5>Modificar Tarjeta
                    <button type="button" id="btnActualizarTarjeta" onclick="actualizarTarjeta()" style="width: 40px; height: 40px;" class="navbar-btn">
                    <i class="fas fa-save"></i>
                    </button>
                    ';
                $formularioModificarTarjeta .=
                    <<<EOD
                    <button type="button" id="btnActualizarTarjeta" onclick="eliminarTarjeta('$idTarj')" style="width: 40px; height: 40px;" class="navbar-btn">
                    <i class="fas fa-trash-alt"></i>
                    </button>
                    EOD;
                $formularioModificarTarjeta .=
                    '
                    </h5>
                    
                        <form id="formModTarjeta" method="POST">
                            <label for="nombre">Ingrese su nombre:</label>
                            <input class="form-control" type="text" name="nombre" value="' . $tarjetas['nombreUsuario'] . '">
                            <label for="numTarjeta">Ingrese el número de tarjeta:</label>
                            <input class="form-control" type="text" name="numTarjeta" value="' . $tarjetas['numeroTarjeta'] . '">
                            <label for="tipo">Ingrese el tipo de tarjeta:</label>
                            <input class="form-control" type="text" name="tipo" value="' . $tarjetas['tipo'] . '">
                            <label for="mesTarjeta">Ingrese el mes de vencimiento:</label>
                            <input class="form-control" type="text" name="mesTarjeta" value="' . $tarjetas['mesVencimiento'] . '">
                            <label for="anioTarjeta">Ingrese el Año de vencimiento:</label>
                            <input class="form-control" type="text" name="anioTarjeta" value="' . $tarjetas['anioVencimiento'] . '">
                            <label for="codigoSeg">Ingrese el código de seguridad:</label>
                            <input class="form-control" type="text" name="codigoSeg" value="' . $tarjetas['codigoSeguridad'] . '">
                            <input class="form-control" type="hidden" name="idTarjeta" value="' . $tarjetas['idTarjeta'] . '">
                        </form>
                    </div>
                ';
                return $formularioModificarTarjeta;
            }
        } else {
            echo "error";
        }
    }
    public function updateTarjeta()
    {

        if (
            !empty($_POST["idTarjeta"]) ||
            !empty($_POST["nombre"]) ||
            !empty($_POST["numTarjeta"]) ||
            !empty($_POST["tipo"]) ||
            !empty($_POST["mesTarjeta"]) ||
            !empty($_POST["anioTarjeta"]) ||
            !empty($_POST["codigoSeg"])
        ) {
            $idTarjeta = addslashes($_POST['idTarjeta']);
            $nombre = addslashes($_POST['nombre']);
            $numTarjeta = addslashes($_POST['numTarjeta']);
            $tipo = addslashes($_POST['tipo']);
            $mesTarjeta = addslashes($_POST['mesTarjeta']);
            $anioTarjeta = addslashes($_POST['anioTarjeta']);
            $codigoSeg = addslashes($_POST['codigoSeg']);
            $cadena = "nombreUsuario = '" . $nombre . "', numeroTarjeta = '" . $numTarjeta . "', tipo = '" . $tipo . "', mesVencimiento = '" . $mesTarjeta . "', anioVencimiento = '" . $anioTarjeta . "', codigoSeguridad = '" . $codigoSeg . "'";

            echo $respuesta = Tarjeta::update($idTarjeta, $cadena);
            // echo $cadena;


        } else {
            header('Location: centros');
        }
    }

    public function deleteTarjeta()
    {

        if (
            !empty($_POST["idTarjeta"])
        ) {
            $idTarjeta = addslashes($_POST['idTarjeta']);
            $respuesta = Tarjeta::delete($idTarjeta);
            echo $respuesta;
            // $respuesta = Tarjeta::update($idTarjeta, $cadena);
            // echo $cadena;


        } else {
            header('Location: centros');
        }
    }

    public function formularioCrearTabla()
    {
        $formularioCrearTabla =
            '
        <div id="crearTabla">
            <h4>Crear Tabla</h4>
            <form id="formCrearTabla" method="POST">
                <label for="nombre">Ingrese el nombre de la Tabla:</label>
                <input class="form-control" type="text" name="nombreTabla">
                <label for="numTarjeta">Ingrese el nombre del campo:</label>
                <input class="form-control" type="text" name="campo">
                <select class="form-control" id="servicio2" name="tipo">
                    <option value="1">texto</option>
                    <option value="2">numero</option>
                    <option value="3">decimal</option>
                    <option value="4">fecha</option>
                </select>
                <button type="button" id="btnCrearTabla" onclick="crearTabla()" style="width: 40px; height: 40px;" class="navbar-btn">

            </form>
        </div>
        ';
        return $formularioCrearTabla;
    }

    public function createTable()
    {
        if (
            !empty($_POST["nombreTabla"])
        ) {
            $nombreTabla = addslashes($_POST['nombreTabla']);
            $campo = addslashes($_POST['campo']);
            $tipo = addslashes($_POST['tipo']);
            $valor = '';
            if ($tipo == 1) {
                $valor = 'VARCHAR(255)';
            } else if ($tipo == 2) {
                $valor = 'INT';
            } else if ($tipo == 3) {
                $valor = 'DOUBLE';
            } else if ($tipo == 2) {
                $valor = 'DATE';
            }
            $campoValor = $campo . ' ' . $valor;
            $respuesta = Tarjeta::createTable($nombreTabla, $campoValor);
            echo $respuesta;
            // $respuesta = Tarjeta::update($idTarjeta, $cadena);
            // echo $cadena;
        } else {
            header('Location: centros');
        }
    }
}
