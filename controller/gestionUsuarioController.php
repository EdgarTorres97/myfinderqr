<?php
require_once ROOT_PATH . '/libs/Controller.php';
require_once ROOT_PATH . '/libs/View.php';
require_once ROOT_PATH . '/model/Centro.php';
require_once ROOT_PATH . '/model/User.php';

class gestionUsuarioController extends Controller
{

    public function gestionUsuariosView()
    {
        return new View('admin/gestionUsuarios_view');
    }


    public function getCentros()
    {
        // $tarjetas = Tarjeta::consultaJSON();
        $centros = Centro::consultaJSON();
        return $centros;
    }

    public function formularioUsuarios()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $formularioUsuario =
                '
            <div id="contenedorFormUsuarios">
            <h5 id="tituloFormUsuarios">Agregar Usuario
            </h5>
            
            <form id="formUsuario" method="POST">
            <label for="name">Ingrese su nombre:</label>
            <input class="form-control" type="text" name="name" id="name" placeholder="Ingrese su nombre">
            <label for="lastName">Ingrese su apellido:</label>
            <input class="form-control" type="text" name="lastName" id="lastName" placeholder="Ingrese el apellido completo">
            <label for="age">Ingrese su edad:</label>
            <input class="form-control" type="text" name="age" id="age" placeholder="Ingrese su edad">
            <label for="mobile">Ingrese su celular:</label>
            <input class="form-control" type="text" name="mobile" id="mobile" placeholder="Ingrese su celular">
            <label for="email">Ingrese su correo electrónico:</label>
            <input class="form-control" type="text" name="email" id="email" placeholder="Ingrese su correo electrónico">
            <label for="user">Ingrese su nombre de usuario:</label>
            <input class="form-control" type="text" name="user" id="email" placeholder="Ingrese su nombre de usuario">
            <label for="password">Ingrese su contraseña:</label>
            <input class="form-control" type="password" name="password" id="password" placeholder="Ingrese su contraseña">
            <label for="passwordV">Repetir contraseña:</label>
            <input class="form-control" type="password" name="passwordV" id="passwordV" placeholder="Repetir contraseña">
            <input  id="btnFormUsuario" onclick="saveUsuario()" class="btn btn-primary" type="button" value="Guardar">

            </form>
            </div>
            
            ';
            return $formularioUsuario;
        } else {
            header('location: login');
        }
    }

    public function saveUsuario()
    {
        if (
            !empty($_POST["name"]) ||
            !empty($_POST["lastName"]) ||
            !empty($_POST["age"]) ||
            !empty($_POST["mobile"]) ||
            !empty($_POST["email"]) ||
            !empty($_POST["user"]) ||
            !empty($_POST["password"])
        ) {
            $name = $_POST['name'];
            $lastName = $_POST['lastName'];
            $age = $_POST['age'];
            $mobile = $_POST['mobile'];
            $email = $_POST['email'];
            $userName = $_POST['user'];
            $password = $_POST['password'];

            $nameExists = userController::verifyUserExists($userName);
            $emailExists = userController::verifyEmailExists($email);

            if ($nameExists == 0 && $emailExists == 0) {
                $user = new User();
                $user->name = addslashes($name);
                $user->lastName = addslashes($lastName);
                $user->age = addslashes($age);
                $user->mobile = addslashes($mobile);
                $user->email = addslashes($email);
                $user->userName = addslashes($userName);
                $user->idRol = 1;
                $user->password = password_hash($password, PASSWORD_DEFAULT, ['cost' => 5]);;
                $user->status = 1;

                $user->createdAt = date("Y-m-d H:i:s");
                echo $user->save();
            } else if ($nameExists == 1) {
                echo 3;
            } else if ($emailExists == 1) {
                echo 4;
            }
        } else {
            echo "falto algún campo";
            header('Location: centros');
        }
    }

    public function buscadorUsuarios()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $buscadorUsuarios =
                '
        <div class="active-cyan-4 mb-4">
            <label for="caja_busqueda">Buscar:</label>
            <input name="caja_busqueda" id="caja_busqueda" class="form-control" type="text" placeholder="Search" aria-label="Buscar">
        </div>
        ';
            return $buscadorUsuarios;
        } else {
            header('location: login');
        }
    }
    public function buscadorUsuariosBaja()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $buscadorUsuarios =
                '
        <div class="active-cyan-4 mb-4">
            <label for="caja_busqueda_Baja">Buscar:</label>
            <input name="caja_busqueda_Baja" id="caja_busqueda_Baja" class="form-control" type="text" placeholder="Search" aria-label="Buscar">
        </div>
        ';
            return $buscadorUsuarios;
        } else {
            header('location: login');
        }
    }
    public function getTableUsuarios()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $usuarios = User::all();
            $tableUsuarios =
                '
            
            <table class="table" border="1" style="">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Usuario</th>
                        <th scope="col">Rol:</th>
                        <th scope="col">Modificar</th>
                        <th scope="col">Baja</th>
                    </tr>
                </thead>
                <tbody id="datos">
                ';
            if (isset($_POST['consulta'])) {
                $consulta = $_POST['consulta'];
                foreach ($usuarios as $recorrido) {
                    if (strpos($recorrido->userName, $consulta) !== false) {
                        if ($recorrido->status == 1 && ($recorrido->idRol == 1 || $recorrido->idRol == 2)) {
                            if ($recorrido->idRol == 1) {
                                $rol = 'administrador';
                            } else if ($recorrido->idRol == 2) {
                                $rol = 'colaborador';
                            } else if ($recorrido->idRol == 3) {
                                $rol = 'usuario';
                            }
                            $tableUsuarios .=
                                '
                    <tr>
                        <td>' . $recorrido->userName . '</td>
                        <td>' . $rol . '</td>                        
                        <td style="text-align: center;"><button type="button"  class="btn btn-warning" onclick="modificarUsuario(' . $recorrido->idUser . ')" style="width: 40px; height: 40px; background-color: yellow;" class="navbar-btn">
                        <i class="fas fa-edit"></i>
                        </button>
                        </td>
                        ';
                            $tableUsuarios .=
                                <<<EOD
                        <td style="text-align: center;"><button type="button" class="btn btn-danger" onclick="eliminar('$recorrido->idUser','$recorrido->userName')" style="width: 40px; height: 40px; background-color: red;" class="navbar-btn">
                        <i class="fas fa-trash-alt"></i>
                        </button>
                        </td>
                    </tr>                            
                    EOD;
                        }
                    }
                }
            } else {
                foreach ($usuarios as $recorrido) {

                    if ($recorrido->status == 1 && ($recorrido->idRol == 1 || $recorrido->idRol == 2)) {
                        if ($recorrido->idRol == 1) {
                            $rol = 'administrador';
                        } else if ($recorrido->idRol == 2) {
                            $rol = 'colaborador';
                        } else if ($recorrido->idRol == 3) {
                            $rol = 'usuario';
                        }
                        $tableUsuarios .=
                            '
                    <tr>
                        <td>' . $recorrido->userName . '</td>
                        <td>' . $rol . '</td>                        
                        <td style="text-align: center;"><button type="button"  class="btn btn-warning" onclick="modificarUsuario(' . $recorrido->idUser . ')" style="width: 40px; height: 40px; background-color: yellow;" class="navbar-btn">
                        <i class="fas fa-edit"></i>
                        </button>
                        </td>
                        ';
                        $tableUsuarios .=
                            <<<EOD
                        <td style="text-align: center;"><button type="button" class="btn btn-danger" onclick="eliminar('$recorrido->idUser','$recorrido->userName')" style="width: 40px; height: 40px; background-color: red;" class="navbar-btn">
                        <i class="fas fa-trash-alt"></i>
                        </button>
                        </td>
                    </tr>                            
                    EOD;
                    }
                }
            }

            $tableUsuarios .=
                '

                </tbody>
            </table>
        ';
            return $tableUsuarios;
        } else {
            header('location: login');
        }
    }

    public function getTableUsuariosBaja()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $usuarios = User::all();
            $tableUsuarios =
                '
            
            <table class="table" border="1" style="">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Usuario</th>
                        <th scope="col">Rol:</th>
                        <th scope="col">Modificar</th>
                        <th scope="col">Alta</th>
                    </tr>
                </thead>
                <tbody id="datos">
                ';
            if (isset($_POST['consulta'])) {
                $consulta = $_POST['consulta'];
                foreach ($usuarios as $recorrido) {
                    if (strpos($recorrido->userName, $consulta) !== false) {
                        if ($recorrido->status == 1 && ($recorrido->idRol == 1 || $recorrido->idRol == 2)) {
                            if ($recorrido->idRol == 1) {
                                $rol = 'administrador';
                            } else if ($recorrido->idRol == 2) {
                                $rol = 'colaborador';
                            } else if ($recorrido->idRol == 3) {
                                $rol = 'usuario';
                            }
                            $tableUsuarios .=
                                '
                    <tr>
                        <td>' . $recorrido->userName . '</td>
                        <td>' . $rol . '</td>                        
                        <td style="text-align: center;"><button type="button"  class="btn btn-warning" onclick="modificarUsuario(' . $recorrido->idUser . ')" style="width: 40px; height: 40px; background-color: yellow;" class="navbar-btn">
                        <i class="fas fa-edit"></i>
                        </button>
                        </td>
                        ';
                            $tableUsuarios .=
                                <<<EOD
                        <td style="text-align: center;"><button type="button" class="btn btn-success" onclick="crearLogicoUsuario('$recorrido->idUser','$recorrido->userName')" style="width: 40px; height: 40px; background-color: #28a745;" class="navbar-btn">
                        <i class="fas fa-arrow-up"></i>
                        </button>
                        </td>
                    </tr>                            
                    EOD;
                        }
                    }
                }
            } else {
                foreach ($usuarios as $recorrido) {

                    if ($recorrido->status == 0 && ($recorrido->idRol == 1 || $recorrido->idRol == 2)) {
                        if ($recorrido->idRol == 1) {
                            $rol = 'administrador';
                        } else if ($recorrido->idRol == 2) {
                            $rol = 'colaborador';
                        } else if ($recorrido->idRol == 3) {
                            $rol = 'usuario';
                        }
                        $tableUsuarios .=
                            '
                    <tr>
                        <td>' . $recorrido->userName . '</td>
                        <td>' . $rol . '</td>                        
                        <td style="text-align: center;"><button type="button"  class="btn btn-warning" onclick="modificarUsuario(' . $recorrido->idUser . ')" style="width: 40px; height: 40px; background-color: yellow;" class="navbar-btn">
                        <i class="fas fa-edit"></i>
                        </button>
                        </td>
                        ';
                        $tableUsuarios .=
                            <<<EOD
                        <td style="text-align: center;"><button type="button" class="btn btn-success" onclick="crearLogicoUsuario('$recorrido->idUser','$recorrido->userName')" style="width: 40px; height: 40px; background-color: #28a745;" class="navbar-btn">
                        <i class="fas fa-arrow-up"></i>
                        </button>
                        </td>
                    </tr>                            
                    EOD;
                    }
                }
            }

            $tableUsuarios .=
                '

                </tbody>
            </table>
        ';
            return $tableUsuarios;
        } else {
            header('location: login');
        }
    }



    public function verifyCentroExists($centro)
    {
        // $tarjetas = Tarjeta::consultaJSON();

        $centro = Centro::all();
        $contador = 0;
        foreach ($centro as $recorrido) {
            if ($contador == 0) {
                if ($recorrido->nombreCentro == $centro) {
                    $contador = 1;
                }
            }
        }
        return $contador;
    }

    public function formularioUpdateUsuarios()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            if (
                !empty($_POST["idUser"])
            ) {

                $idUsuario = addslashes($_POST["idUser"]);
                $usuarios = User::id($idUsuario);

                $formularioUpdateUsuarios =
                    '
            <div id="contenedorFormUpdateCentros">
            <h1 id="tituloFormUpdateCentro" class="titulo">
            Modificar centro
            <i class="fas fa-minus-circle" onclick="ocultarFormUpdateUsuarios()" style="color: red;"></i>
            </h1>
            
            <h3>Usuario: ' . $usuarios['userName'] . '</h3>
            <form id="formUpdateUsuario" method="POST">

            <label for="name">Ingrese su nombre:</label>
            <input class="form-control" type="text" name="name" id="name" value="' . $usuarios['name'] . '">
            <label for="lastName">Ingrese su apellido:</label>
            <input class="form-control" type="text" name="lastName" id="lastName" value="' . $usuarios['lastName'] . '">
            <label for="age">Ingrese su edad:</label>
            <input class="form-control" type="text" name="age" id="age" value="' . $usuarios['age'] . '">
            <label for="mobile">Ingrese su celular:</label>
            <input class="form-control" type="text" name="mobile" id="mobile" value="' . $usuarios['mobile'] . '">
            <label for="email">Ingrese su correo electrónico:</label>
            <input class="form-control" type="text" name="email" id="email" value="' . $usuarios['email'] . '">
            <input class="form-control" type="hidden" id="idUser" name="idUser" value="' . $usuarios['idUser'] . '">
            <input  id="btnFormUpdateUser" onclick="updateUsuario()" class="btn btn-primary" type="button" value="Guardar">
            </form>
            </div>
            <div id="cambiarClave">
            <form id="cambiarContra" method="POST">
                <h2>Cambio de contraseña</h2>
                
                <label for="claven">Clave nueva:</label>
                <input class="form-control" id="claven" type="password" name="claven" required>
                <label for="rclaven">Repetir clave nueva:</label>
                <input class="form-control" id="rclaven" type="password" name="rclaven" required>
                <input class="form-control" type="hidden" id="idUserCambiar" name="idUser" value="' . $usuarios['idUser'] . '">
                <input  id="btnFormCambiarClave" onclick="cambiarClave()" class="btn btn-primary" type="button" value="Cambiar contraseña">
            </form>
            
            </div>
            ';
                return $formularioUpdateUsuarios;
            }
        } else {
            header('location: login');
        }
    }

    public function updateUsuario()
    {

        if (
            !empty($_POST["idUser"]) ||
            !empty($_POST["name"]) ||
            !empty($_POST["lastName"]) ||
            !empty($_POST["age"]) ||
            !empty($_POST["mobile"]) ||
            !empty($_POST["email"])
        ) {
            $idUsuario = addslashes($_POST["idUser"]);
            $name = addslashes($_POST['name']);
            $lastName = addslashes($_POST['lastName']);
            $age = addslashes($_POST['age']);
            $mobile = addslashes($_POST['mobile']);
            $email = addslashes($_POST['email']);

            $cadena = "name = '" . $name . "', lastName = '" . $lastName . "', age = '" . $age . "', mobile = '" . $mobile . "', email = '" . $email . "'";

            $respuesta = User::update($idUsuario, $cadena);
            echo $respuesta;
        } else {
            header('Location: centros');
        }
    }

    public function deleteLogicoUsuario()
    {

        if (
            !empty($_POST["idUser"])
        ) {
            $idUser = addslashes($_POST['idUser']);
            $usuarios = User::id($idUser);
            $respuesta = 0;
            if ($usuarios['idRol'] != 1) {
                $cadena = "status = 0";

                $respuesta = User::update($idUser, $cadena);
            } else {
                $respuesta = 3;
            }
            echo $respuesta;
        } else {
            header('Location: centros');
        }
    }
    public function createLogicoUsuario()
    {

        if (
            !empty($_POST["idUser"])
        ) {
            $idUser = addslashes($_POST['idUser']);
            $usuarios = User::id($idUser);
            $respuesta = 0;
            if ($usuarios['idRol'] != 1) {
                $cadena = "status = 1";

                $respuesta = User::update($idUser, $cadena);
            } else {
                $respuesta = 3;
            }
            echo $respuesta;
        } else {
            header('Location: centros');
        }
    }

    public function deleteCentro()
    {

        if (
            !empty($_POST["idCentro"])
        ) {
            $idCentro = addslashes($_POST['idCentro']);
            $respuesta = Centro::delete($idCentro);
            echo $respuesta;
            // $respuesta = Tarjeta::update($idTarjeta, $cadena);
            // echo $cadena;


        } else {
            header('Location: centros');
        }
    }

    public function cambiarClave()
    {
        if (
            !empty($_POST["idUser"]) && !empty($_POST["claven"])

        ) {
            $idUser = $_POST["idUser"];
            $claven = $_POST['claven'];

            $claven = password_hash($claven, PASSWORD_DEFAULT, ['cost' => 5]);

            $cadena = "password = '" . $claven . "'";
            echo User::update($idUser,$cadena);
        } else {
            header('Location: login');
        }
    }
}
