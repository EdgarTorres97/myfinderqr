<?php
require_once ROOT_PATH . '/libs/Controller.php';
require_once ROOT_PATH . '/libs/View.php';
require_once ROOT_PATH . '/model/Centro.php';
require_once ROOT_PATH . '/model/User.php';

class userController extends Controller
{

    public function loginView()
    {
        return new View('login/login_view');
    }

    public function RegisterView()
    {
        return new View('login/register_view');
    }

    public function formRegistro()
    {
        $formulario = '
        <h1>Registro</h1>
        <form id="formRegistro" method="POST">
        <label for="name">Ingrese su nombre:</label>
        <input class="form-control" type="text" name="name" id="name" placeholder="Ingrese su nombre">
        <label for="lastName">Ingrese su apellido:</label>
        <input class="form-control" type="text" name="lastName" id="lastName" placeholder="Ingrese el apellido completo">
        <label for="age">Ingrese su edad:</label>
        <input class="form-control" type="text" name="age" id="age" placeholder="Ingrese su edad">
        <label for="mobile">Ingrese su celular:</label>
        <input class="form-control" type="text" name="mobile" id="mobile" placeholder="Ingrese su celular">
        <label for="email">Ingrese su correo electrónico:</label>
        <input class="form-control" type="text" name="email" id="email" placeholder="Ingrese su correo electrónico">
        <label for="user">Ingrese su nombre de usuario:</label>
        <input class="form-control" type="text" name="user" id="user" placeholder="Ingrese su nombre de usuario">
        <label for="password">Ingrese su contraseña:</label>
        <input class="form-control" type="password" name="password" id="password" placeholder="Ingrese su contraseña">
        <label for="passwordV">Repetir contraseña:</label>
        <input class="form-control" type="password" name="passwordV" id="passwordV" placeholder="Repetir contraseña">
        <input  id="btnFormRegistro" onclick="formRegistro()" class="btn btn-primary" type="button" value="Enviar">
        </form>';
        return $formulario;
    }

    public function saveRegistro()
    {
        if (
            !empty($_POST["name"]) ||
            !empty($_POST["lastName"]) ||
            !empty($_POST["age"]) ||
            !empty($_POST["mobile"]) ||
            !empty($_POST["email"]) ||
            !empty($_POST["user"]) ||
            !empty($_POST["password"])
        ) {
            $name = $_POST['name'];
            $lastName = $_POST['lastName'];
            $age = $_POST['age'];
            $mobile = $_POST['mobile'];
            $email = $_POST['email'];
            $userName = $_POST['user'];
            $password = $_POST['password'];

            $nameExists = userController::verifyUserExists($userName);
            $emailExists = userController::verifyEmailExists($email);

            if ($nameExists == 0 && $emailExists == 0) {
                $user = new User();
                $user->name = addslashes($name);
                $user->lastName = addslashes($lastName);
                $user->age = addslashes($age);
                $user->mobile = addslashes($mobile);
                $user->email = addslashes($email);
                $user->userName = addslashes($userName);
                $user->idRol = 2;
                $user->password = password_hash($password, PASSWORD_DEFAULT, ['cost' => 5]);;
                $user->status = 1;

                $user->createdAt = date("Y-m-d H:i:s");
                echo $user->save();
            } else if ($nameExists == 1) {
                echo 3;
            } else if ($emailExists == 1) {
                echo 4;
            }
        } else {
            echo "falto algún campo";
            header('Location: centros');
        }
    }

    public function verifyUserExists($user)
    {
        // $tarjetas = Tarjeta::consultaJSON();

        $users = User::all();
        $contador = 0;
        foreach ($users as $recorrido) {
            if ($contador == 0) {
                if ($recorrido->userName == $user) {
                    $contador = 1;
                }
            }
        }
        return $contador;
    }

    public function verifyEmailExists($email)
    {
        // $tarjetas = Tarjeta::consultaJSON();

        $users = User::all();
        $contador = 0;
        foreach ($users as $recorrido) {
            if ($contador == 0) {
                if ($recorrido->email == $email) {
                    $contador = 1;
                }
            }
        }
        return $contador;
    }
    public function formLogin()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $formulario = '
        <h1>Login</h1>
        <form id="formLogin" action="valida" method="POST">
        <label for="user">Ingrese su nombre de usuario:</label>
        <input class="form-control" type="text" name="user" id="user" placeholder="Ingrese su nombre de usuario">
        <label for="password">Ingrese su contraseña:</label>
        <input class="form-control" type="password" name="password" id="password" placeholder="Ingrese su contraseña">
        <div class="btn__form">
            <input id="btnIngresar" class="btn btn-primary" type="submit" value="Ingresar">
        </div>

        </form>';
            return $formulario;
        } else {
            header('location: login');
        }
    }

    public function valida()
    {
        if (
            !empty($_POST["user"]) ||
            !empty($_POST["password"])
        ) {
            $userName = $_POST['user'];
            $password = $_POST['password'];
            $contador = 0;
            $usuarios = User::all();

            foreach ($usuarios as $recorrido) {
                $userNameBd = $recorrido->userName;
                $statusBd = $recorrido->status;
                if ($userNameBd == $userName && $statusBd == 1) {

                    if (password_verify($password, $recorrido->password)) {
                        setcookie('cookie-name', '1', 0, '/; samesite=strict');
                        $_SESSION['userId'] = $recorrido->idUser;
                        $_SESSION['userName'] = $recorrido->userName;
                        $_SESSION['name'] = $recorrido->name;
                        $_SESSION['lastName'] = $recorrido->lastName;
                        $_SESSION['age'] = $recorrido->age;
                        $_SESSION['mobile'] = $recorrido->mobile;
                        $_SESSION['email'] = $recorrido->email;
                        $_SESSION['rolId'] = $recorrido->idRol;
                        $contador++;
                    }
                }
            }
            if ($contador == 0) {
                echo "
                <script src='js/jquery-3.4.1.min.js'></script>
                <script src='js/sweetalert2.all.min.js'></script>
                <script>
                $(document).ready(function () {
                    Swal.fire({
                        icon: 'error',
                        title: 'Usuario o contraseña incorrecto',
                    }).then((result) => {
                        if (result.value) {
                
                            window.history.go(-1);
                
                            return false;
                        }
                        window.history.go(-1);
                    });
                });
                
                
                </script>";
            } else {
                header('location: centros');
            }
        } else {
            echo "falto algún campo";
            header('Location: centros');
        }
    }


    public function endSesion()
    {
        session_destroy();
        header('location: login');
    }

    public function perfil()
    {
        return new View('users/perfil_view');
    }

    public function formUpdatePerfil()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {


            $perfil = User::id($_SESSION['userId']);
            $formularioUpdatePerfil =
                '
        <form id="formModificarPerfil" method="POST">
            <label for="name">Nombre:</label>
            <input class="form-control" type="text" name="name" id="name" value="' . $perfil['name'] . '">
            <label for="lastName">Apellido:</label>
            <input class="form-control" type="text" name="lastName" id="lastName" value="' . $perfil['lastName'] . '">
            <label for="age">Edad:</label>
            <input class="form-control" type="text" name="age" id="age" value="' . $perfil['age'] . '">
            <label for="mobile">Celular:</label>
            <input class="form-control" type="text" name="mobile" id="mobile" value="' . $perfil['mobile'] . '">
            <label for="email">Correo electrónico:</label>
            <input class="form-control" type="text" name="email" id="email" value="' . $perfil['email'] . '">
            <input  id="btnFormModificarPerfiil" onclick="modificarPerfil()" class="btn btn-primary" type="button" value="Enviar">
        </form>
        ';
            return $formularioUpdatePerfil;
        } else {
            header('location: login');
        }
    }

    public function formUpdatePassword()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {


            $perfil = User::id($_SESSION['userId']);
            $formularioUpdatePassword =
                '
        <form id="formModificarPassword" method="POST">
                    <h2>Cambio de contraseña</h2>
                    <label for="nombre">Clave antigua:</label>
                    <input class="form-control" id="clave" type="password" name="clave" required>
                    <label for="apellidop">Clave nueva:</label>
                    <input class="form-control" id="claven" type="password" name="claven" required>
                    <label for="apellidom">Repetir clave nueva:</label>
                    <input class="form-control" id="rclaven" type="password" name="rclaven" required>
                    <input  id="btnFormModificarPassword" onclick="modificarPassword()" class="btn btn-primary" type="button" value="Enviar">

                </form>
        ';
            return $formularioUpdatePassword;
        } else {
            header('location: login');
        }
    }
    public function updatePerfilUsuario()
    {

        if (
            !empty($_POST["name"]) ||
            !empty($_POST["lastName"]) ||
            !empty($_POST["age"]) ||
            !empty($_POST["mobile"]) ||
            !empty($_POST["email"])
        ) {
            $idUsuario = $_SESSION['userId'];
            $name = addslashes($_POST['name']);
            $lastName = addslashes($_POST['lastName']);
            $age = addslashes($_POST['age']);
            $mobile = addslashes($_POST['mobile']);
            $email = addslashes($_POST['email']);

            $cadena = "name = '" . $name . "', lastName = '" . $lastName . "', age = '" . $age . "', mobile = '" . $mobile . "', email = '" . $email . "'";

            $respuesta = User::update($idUsuario, $cadena);
            echo $respuesta;
        } else {
            header('Location: centros');
        }
    }
    public function updatePasswordUsuario()
    {
        if (
            !empty($_POST["claven"]) ||
            !empty($_POST["clave"])

        ) {
            $idUsuario = $_SESSION['userId'];
            $clave = $_POST["clave"];
            $claven = $_POST['claven'];
            $registro = User::id($idUsuario);

            $claven = $claven;

            if (password_verify($clave, $registro['password'])) {
                $claven = password_hash($claven, PASSWORD_DEFAULT, ['cost' => 5]);
                $cadena = "password = '" . $claven . "'";
                $respuesta = User::update($idUsuario, $cadena);
                echo $respuesta;
            } else {
                echo 5;
            }
        } else {
            header('Location: login');
        }
    }
}
