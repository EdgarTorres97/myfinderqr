<?php
require_once ROOT_PATH . '/libs/Controller.php';
require_once ROOT_PATH . '/libs/View.php';
require_once ROOT_PATH . '/model/Centro.php';
require_once ROOT_PATH . '/model/ComentariosCentros.php';


class gestionCentroController extends Controller
{

    public function mapView()
    {
        return new View('users/mapa_view');
    }

    public function centrosView()
    {
        return new View('admin/gestionCentros_view');
    }
    public function informacionCentroView()
    {
        return new View('users/informacionCentro_view');
    }
    public function infoCentroView()
    {
        $idCentro = $_GET['centro'];
        $idCentro = addslashes($idCentro);

        $centro = Centro::id($idCentro);
        $comentario = ComentariosCentros::foreignKey($idCentro,'idCentro');
        // $comentario = ComentariosCentros::all();

        if ($centro != 2) {
            // echo $centro['nombreCentro'];
            return new View('users/infoCentro_view', ['centro' => $centro,'comentario'=>$comentario]);
        } else {
            header('location: login');
        }
    }
    public function formularioCentros()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $formularioCentros =
                '
            <div id="contenedorFormCentros">
            <h5 id="tituloFormCentros">Agregar Centro
            </h5>
            
            <form id="formCentro" method="POST" enctype="multipart/form-data" >
                <label for="nombre">Ingrese el nombre del centro</label>
                <input class="form-control" type="text" id="nombreCentro" name="nombreCentro" placeholder="Ingrese el nombre del centro">
                <label for="numero">Ingrese el número de teléfono o celular:</label>
                <input class="form-control" type="text" id="numero" name="numero" placeholder="Ingrese el número de teléfono o celular">
                <label for="estado">Ingrese el estado de la republica donde se encuentra:</label>
                <input class="form-control" type="text" id="estado" name="estado" placeholder="Ingrese el estado de la republica donde se encuentra">
                <label for="horario">Ingrese el horario:</label>
                <input class="form-control" type="text" id="horario" name="horario" placeholder="Ingrese el horario">
                <label for="ubicacion">Ingrese la ubicación en google maps:</label>
                <input class="form-control" type="text" id="ubicacion" name="ubicacion" placeholder="Ingrese la ubicación en google maps">
                <label for="descripcion">Ingrese la descripcion:</label>
                <input class="form-control" type="text" id="descripcion" name="descripcion" placeholder="Ingrese la descripcion en google maps">
                <label for="imagen">Ingrese la imagen(opcional):</label><br>
                <input class="btn btn-secondary" id="imagen" name="imagen" type="file" />
                <br>
                <label for="tipo">Ingrese el tipo de centro:</label>
                <input class="form-control" type="text" id="tipo" name="tipo" placeholder="Ingrese el tipo de centro">
                <label for="latitud">Ingrese la latitud:</label>
                <input class="form-control" type="text" id="latitud" name="latitud" placeholder="Ingrese la latitud">
                <label for="longitud">Ingrese la longitud:</label>
                <input class="form-control" type="text" id="longitud" name="longitud" placeholder="Ingrese la longitud">
                <input  id="btnFormCentro" onclick="saveCentro()" class="btn btn-primary" type="button" value="Guardar">

            </form>
            </div>
            
            ';
            return $formularioCentros;
        } else {
            header('location: login');
        }
    }
    public function getCentros()
    {
        // $tarjetas = Tarjeta::consultaJSON();
        $centros = Centro::consultaJSON();
        return $centros;
    }

    public function buscadorCentros()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $buscadorCentros =
                '
                <h3>Activos</h3>
        <div class="active-cyan-4 mb-4">
            <label for="caja_busqueda">Buscar:</label>
            <input name="caja_busqueda" id="caja_busqueda" class="form-control" type="text" placeholder="Search" aria-label="Buscar">
        </div>
        ';
            return $buscadorCentros;
        } else {
            header('location: login');
        }
    }

    public function getInfoCentros()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $centros = Centro::all();
            $infoCentros = "";
            foreach ($centros as $recorrido) {
                if ($recorrido->status == 1) {
                    $infoCentros .=
                        '
                <div class="informacionCentros card">
                    <div class="card-header">
                        ' . $recorrido->nombreCentro . '
                        
                    </div>
                    <div class="card-body">
                        <p class="card-text">Horario de atención: ' . $recorrido->horario . '</p>
                        <p class="card-text">Número de contácto: ' . $recorrido->estado . '</p>
                        <p class="card-text">Tipo de centro: ' . $recorrido->tipo . '</p>
                        <a href="' . $recorrido->ubicacion . '" class="btn btn-primary">Ubicación</a>
                    </div>
                </div>
                ';
                }
            }
            return $infoCentros;
        } else {
            header('location: login');
        }
    }

    public function getTableCentros()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $centros = Centro::all();
            $tableCentros =
                '
            
            <table class="table" border="1" style="">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Estado:</th>
                        <th scope="col">ubicacion</th>
                        <th scope="col">Modificar</th>
                        <th scope="col">Baja</th>
                    </tr>
                </thead>
                <tbody id="datos">
                ';
            if (isset($_POST['consulta'])) {
                $consulta = $_POST['consulta'];
                foreach ($centros as $recorrido) {
                    if (strpos($recorrido->nombreCentro, $consulta) !== false) {
                        if ($recorrido->status == 1) {
                            $tableCentros .=
                                '
                    <tr>
                        <td>' . $recorrido->nombreCentro . '</td>
                        <td>' . $recorrido->estado . '</td>
                        <td style="text-align: center;"><a class="btn btn-primary" href="' . $recorrido->ubicacion . '" role="button"><i class="fas fa-map-marked-alt"></i></a></td>
                        
                        <td style="text-align: center;"><button type="button"  class="btn btn-warning" onclick="modificarCentro(' . $recorrido->idCentro . ')" style="width: 40px; height: 40px; background-color: yellow;" class="navbar-btn">
                        <i class="fas fa-edit"></i>
                        </button>
                        </td>
                    ';
                            $tableCentros .=
                                <<<EOD
                                    <td style="text-align: center;"><button type="button" class="btn btn-danger" onclick="deleteLogicoCentro('$recorrido->idCentro','$recorrido->nombreCentro')" style="width: 40px; height: 40px; background-color: red;" class="navbar-btn">
                                    <i class="fas fa-trash-alt"></i>
                                    </button>
                                    </td>
                                </tr>                            
                                EOD;
                        }
                    }
                }
            } else {
                foreach ($centros as $recorrido) {
                    if ($recorrido->status == 1) {
                        $tableCentros .=
                            '
                    <tr>
                        <td>' . $recorrido->nombreCentro . '</td>
                        <td>' . $recorrido->estado . '</td>
                        <td style="text-align: center;"><a class="btn btn-primary" href="' . $recorrido->ubicacion . '" role="button"><i class="fas fa-map-marked-alt"></i></a></td>
                        <td style="text-align: center;"><button type="button" class="btn btn-warning" onclick="modificarCentro(' . $recorrido->idCentro . ')" style="width: 40px; height: 40px; background-color: yellow;" class="navbar-btn">
                        <i class="fas fa-edit"></i>
                        </button>
                        </td>
                    ';
                        $tableCentros .=
                            <<<EOD
                        <td style="text-align: center;"><button type="button" class="btn btn-danger" onclick="deleteLogicoCentro('$recorrido->idCentro','$recorrido->nombreCentro')" style="width: 40px; height: 40px; background-color: red;" class="navbar-btn">
                        <i class="fas fa-trash-alt"></i>
                        </button>
                        </td>
                    </tr>                            
                    EOD;
                    }
                }
            }

            $tableCentros .=
                '

                </tbody>
            </table>
        ';
            return $tableCentros;
        } else {
            header('location: login');
        }
    }
    public function buscadorCentrosBaja()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $buscadorCentros =
                '
                <h3>Inactivos</h3>
        <div class="active-cyan-4 mb-4">
            <label for="caja_busqueda_baja">Buscar:</label>
            <input name="caja_busqueda_baja" id="caja_busqueda_baja" class="form-control" type="text" placeholder="Search" aria-label="Buscar">
        </div>
        ';
            return $buscadorCentros;
        } else {
            header('location: login');
        }
    }
    public function getTableCentrosBaja()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $centros = Centro::all();
            $tableCentros =
                '
            
            <table class="table" border="1" style="">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Estado:</th>
                        <th scope="col">ubicacion</th>
                        <th scope="col">Modificar</th>
                        <th scope="col">Alta</th>
                    </tr>
                </thead>
                <tbody id="datosBaja">
                ';
            if (isset($_POST['consulta'])) {
                $consulta = $_POST['consulta'];
                foreach ($centros as $recorrido) {
                    if (strpos($recorrido->nombreCentro, $consulta) !== false) {
                        if ($recorrido->status == 0) {
                            $tableCentros .=
                                '
                    <tr>
                        <td>' . $recorrido->nombreCentro . '</td>
                        <td>' . $recorrido->estado . '</td>
                        <td style="text-align: center;"><a class="btn btn-primary" href="' . $recorrido->ubicacion . '" role="button"><i class="fas fa-map-marked-alt"></i></a></td>
                        
                        <td style="text-align: center;"><button type="button"  class="btn btn-warning" onclick="modificarCentro(' . $recorrido->idCentro . ')" style="width: 40px; height: 40px; background-color: yellow;" class="navbar-btn">
                        <i class="fas fa-edit"></i>
                        </button>
                        </td>
                    ';
                            $tableCentros .=
                                <<<EOD
                        <td style="text-align: center;"><button type="button" class="btn btn-success" onclick="createLogicoCentro('$recorrido->idCentro','$recorrido->nombreCentro')" style="width: 40px; height: 40px; background-color: #28a745;" class="navbar-btn">
                        <i class="fas fa-arrow-up"></i>
                        </button>
                        </td>
                    </tr>                            
                    EOD;
                        }
                    }
                }
            } else {
                foreach ($centros as $recorrido) {
                    if ($recorrido->status == 0) {
                        $tableCentros .=
                            '
                    <tr>
                        <td>' . $recorrido->nombreCentro . '</td>
                        <td>' . $recorrido->estado . '</td>
                        <td style="text-align: center;"><a class="btn btn-primary" href="' . $recorrido->ubicacion . '" role="button"><i class="fas fa-map-marked-alt"></i></a></td>
                        <td style="text-align: center;"><button type="button" class="btn btn-warning" onclick="modificarCentro(' . $recorrido->idCentro . ')" style="width: 40px; height: 40px; background-color: yellow;" class="navbar-btn">
                        <i class="fas fa-edit"></i>
                        </button>
                        </td>
                    ';
                        $tableCentros .=
                            <<<EOD
                        <td style="text-align: center;"><button type="button" class="btn btn-success" onclick="createLogicoCentro('$recorrido->idCentro','$recorrido->nombreCentro')" style="width: 40px; height: 40px; background-color: #28a745;" class="navbar-btn">
                        <i class="fas fa-arrow-up"></i>
                        </button>
                        </td>
                    </tr>                            
                    EOD;
                    }
                }
            }

            $tableCentros .=
                '

                </tbody>
            </table>
        ';
            return $tableCentros;
        } else {
            header('location: login');
        }
    }


    public function saveCentro()
    {
        if (
            !empty($_POST["nombreCentro"]) ||
            !empty($_POST["numero"]) ||
            !empty($_POST["estado"]) ||
            !empty($_POST["horario"]) ||
            !empty($_POST["ubicacion"]) ||
            !empty($_POST["descripcion"]) ||
            !empty($_POST["tipo"]) ||
            !empty($_POST["latitud"]) ||
            !empty($_POST["longitud"])
        ) {

            $nombreCentro = $_POST['nombreCentro'];
            $numero = $_POST['numero'];
            $estado = $_POST['estado'];
            $horario = $_POST['horario'];
            $ubicacion = $_POST['ubicacion'];
            $descripcion = $_POST['descripcion'];

            $tipo = $_POST['tipo'];
            $latitud = $_POST['latitud'];
            $longitud = $_POST['longitud'];
            
                
                $ruta_temporal = $_FILES['imagen']['tmp_name'];
                $nombreArchivo = $_FILES['imagen']['name'];
                $soloelnombre = explode('.', $nombreArchivo);
                $laextension = strtolower(end($soloelnombre));
                $extensionesValidas = array('jpeg', 'jpg', 'png');
                if (in_array($laextension, $extensionesValidas)) {
                    $directorio = 'styles/images/centros/';
                    $nuevoNombre = "centro_" . time() . $nombreArchivo;
                    $rutaDestino = $directorio . $nuevoNombre;
                    if (move_uploaded_file($ruta_temporal, $rutaDestino)) {
                        $nameExists = gestionCentroController::verifyCentroExists($nombreCentro);

                        if ($nameExists == 0) {

                            $centro = new Centro();
                            $centro->nombreCentro = addslashes($nombreCentro);
                            $centro->numero = addslashes($numero);
                            $centro->estado = addslashes($estado);
                            $centro->horario = addslashes($horario);
                            $centro->ubicacion = $ubicacion;
                            $centro->descripcion = addslashes($descripcion);
                            $centro->tipo = addslashes($tipo);
                            $centro->imagen = $rutaDestino;
                            $centro->latitud = addslashes($latitud);
                            $centro->longitud = addslashes($longitud);
                            $centro->visitas = 0;
                            $centro->status = 1;

                            $centro->createdAt = date("Y-m-d H:i:s");
                            echo $centro->save();
                        } else if ($nameExists == 1) {
                            echo 3;
                        }
                    } else {
                        return 2;
                    }
                }else{
                    
                    $imagen = 'styles/images/centros/centroacopio.jpg';
                $nameExists = gestionCentroController::verifyCentroExists($nombreCentro);

                        if ($nameExists == 0) {

                            $centro = new Centro();
                            $centro->nombreCentro = addslashes($nombreCentro);
                            $centro->numero = addslashes($numero);
                            $centro->estado = addslashes($estado);
                            $centro->horario = addslashes($horario);
                            $centro->ubicacion = $ubicacion;
                            $centro->descripcion = addslashes($descripcion);
                            $centro->tipo = addslashes($tipo);
                            $centro->imagen = $imagen;
                            $centro->latitud = addslashes($latitud);
                            $centro->longitud = addslashes($longitud);
                            $centro->visitas = 0;
                            $centro->status = 1;

                            $centro->createdAt = date("Y-m-d H:i:s");
                            echo $centro->save();
                        } else if ($nameExists == 1) {
                            echo 3;
                        }
                }
            
        } else {
            echo "falto algún campo";
            header('Location: centros');
        }
    }

    public function verifyCentroExists($centro)
    {
        // $tarjetas = Tarjeta::consultaJSON();

        $centro = Centro::all();
        $contador = 0;
        foreach ($centro as $recorrido) {
            if ($contador == 0) {
                if ($recorrido->nombreCentro == $centro) {
                    $contador = 1;
                }
            }
        }
        return $contador;
    }

    public function formularioUpdateCentros()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            if (
                !empty($_POST["idCentro"])
            ) {

                $idCentro = addslashes($_POST["idCentro"]);
                $centros = Centro::id($idCentro);

                $formularioUpdateCentros =
                    '
            <div id="contenedorFormUpdateCentros">
            <h1 id="tituloFormUpdateCentro" class="titulo">
            Modificar centro
            <i class="fas fa-minus-circle" onclick="ocultarFormUpdateCentros()" style="color: red;"></i>
            </h1>
            
            <form id="formUpdateCentro" method="POST">
                <label for="nombre">Ingrese el nombre del centro</label>
                <input class="form-control" type="text" id="nombreCentro" name="nombreCentro" value="' . $centros['nombreCentro'] . '">
                <label for="numero">Ingrese el número de teléfono o celular:</label>
                <input class="form-control" type="text" id="numero" name="numero" value="' . $centros['numero'] . '">
                <label for="estado">Ingrese el estado de la republica donde se encuentra:</label>
                <input class="form-control" type="text" id="estado" name="estado" value="' . $centros['estado'] . '">
                <label for="horario">Ingrese el horario:</label>
                <input class="form-control" type="text" id="horario" name="horario" value="' . $centros['horario'] . '">
                <label for="ubicacion">Ingrese la ubicación en google maps:</label>
                <input class="form-control" type="text" id="ubicacion" name="ubicacion" value="' . $centros['ubicacion'] . '">
                <img style="width: 40%; max-height: 100px" src="' . $centros['imagen'] . '" alt="imagen del centro">
                <br>
                <label for="tipo">Ingrese el tipo de centro:</label>
                <input class="form-control" type="text" id="tipo" name="tipo" value="' . $centros['tipo'] . '">
                <label for="latitud">Ingrese la latitud:</label>
                <input class="form-control" type="text" id="latitud" name="latitud" value="' . $centros['latitud'] . '">
                <label for="longitud">Ingrese la longitud:</label>
                <input class="form-control" type="text" id="longitud" name="longitud" value="' . $centros['longitud'] . '">
                <input class="form-control" type="hidden" id="idCentro" name="idCentro" value="' . $centros['idCentro'] . '">
                <input  id="btnFormUpdateCentro" onclick="updateCentro()" class="btn btn-primary" type="button" value="Guardar">

            </form>
            </div>
            
            ';
                return $formularioUpdateCentros;
            }
        } else {
            header('location: login');
        }
    }

    public function updateCentro()
    {

        if (
            !empty($_POST["idCentro"]) ||
            !empty($_POST["nombreCentro"]) ||
            !empty($_POST["numero"]) ||
            !empty($_POST["estado"]) ||
            !empty($_POST["horario"]) ||
            !empty($_POST["ubicacion"]) ||
            !empty($_POST["tipo"]) ||
            !empty($_POST["latitud"]) ||
            !empty($_POST["longitud"])
        ) {
            $idCentro = addslashes($_POST["idCentro"]);
            $nombreCentro = $_POST['nombreCentro'];
            $numero = $_POST['numero'];
            $estado = $_POST['estado'];
            $horario = $_POST['horario'];
            $ubicacion = $_POST['ubicacion'];
            $tipo = $_POST['tipo'];
            $latitud = $_POST['latitud'];
            $longitud = $_POST['longitud'];

            $cadena = "nombreCentro = '" . $nombreCentro . "', numero = '" . $numero . "', estado = '" . $estado . "', horario = '" . $horario . "', ubicacion = '" . $ubicacion . "', tipo = '" . $tipo . "', latitud = '" . $latitud . "', longitud = '" . $longitud . "'";

            echo $respuesta = Centro::update($idCentro, $cadena);
            // echo $cadena;


        } else {
            header('Location: centros');
        }
    }

    public function deleteLogicoCentro()
    {

        if (
            !empty($_POST["idCentro"])
        ) {
            $idCentro = addslashes($_POST['idCentro']);
            $cadena = "status = 0";

            $respuesta = Centro::update($idCentro, $cadena);
            echo $respuesta;
        } else {
            header('Location: centros');
        }
    }
    public function createLogicoCentro()
    {

        if (
            !empty($_POST["idCentro"])
        ) {
            $idCentro = addslashes($_POST['idCentro']);
            $cadena = "status = 1";

            $respuesta = Centro::update($idCentro, $cadena);
            echo $respuesta;
        } else {
            header('Location: centros');
        }
    }

    public function deleteCentro()
    {

        if (
            !empty($_POST["idCentro"])
        ) {
            $idCentro = addslashes($_POST['idCentro']);
            $respuesta = Centro::delete($idCentro);
            echo $respuesta;
            // $respuesta = Tarjeta::update($idTarjeta, $cadena);
            // echo $cadena;


        } else {
            header('Location: centros');
        }
    }
}
