<?php
require_once ROOT_PATH . '/libs/Controller.php';
require_once ROOT_PATH . '/libs/View.php';
require_once ROOT_PATH . '/model/Centro.php';
require_once ROOT_PATH . '/model/ComentariosCentros.php';
require_once ROOT_PATH . '/model/GustasComentarioCentro.php';
require_once ROOT_PATH . '/model/NoGustasComentarioCentro.php';


class comentariosCentrosController extends Controller
{

    public function infoCentroView()
    {
        $idCentro = $_GET['centro'];
        $idCentro = addslashes($idCentro);

        $centro = Centro::id($idCentro);

        // $comentario = ComentariosCentros::all();

        if ($centro != 2) {
            // echo $centro['nombreCentro'];
            return new View('users/infoCentro_view');
        } else {
            header('location: login');
        }
    }

    public function cardInfoCentor()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            if (isset($_POST['idCentro'])) {
                $idCentro = $_POST['idCentro'];
                $centro = Centro::id($idCentro);
                $cardComentarios =
                    '
                <div class="card mb-3">
                        <img style="width: 20%;" src="' . $centro['imagen'] . '" alt="">
                        <div class="card-body">
                            <h5 class="card-title">Nombre del centro:' . $centro['nombreCentro'] . '</h5>
                            <p class="card-text">Tipo de centro:' . $centro['tipo'] . '</p>
        
                            <p class="card-text">Número de contácto:' . $centro['numero'] . '</p>
                            <p class="card-text">Horario: ' . $centro['horario'] . '</p>
                            <a href="' . $centro['ubicacion'] . '" class="btn btn-primary">Ubicación</a>
        
                        </div>
                    </div>
                ';
                return $cardComentarios;
            } else {
                header('location: login');
            }
        } else {
            header('location: login');
        }
    }


    function comentarios()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            if (isset($_POST['idCentro'])) {
                $idCentro = $_POST['idCentro'];
                $centro = Centro::id($idCentro);
                $comentario = ComentariosCentros::foreignKey($idCentro, 'idCentro');
                $comen = '';
                foreach ($comentario as $recorrido) {
                    $comen .=
                        '
                        <div class="card">
                            <div class="card-header">
                            Título:' . $recorrido->titulo . '
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Special title treatment</h5>
                                <p class="card-text">Comentario: ' . $recorrido->comentario . '</p>
                                <p>Fecha: ' . $recorrido->fecha . '</p>
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                        <td><i style="float: left;" class="fas fa-heart" onclick="like(' . $recorrido->idComentario . ')"></i><p id="like' . $recorrido->idComentario . '">' . $recorrido->gustar . '</p></td>
                                        <td><i style="float: left;" class="fas fa-heart-broken" onclick="dislike(' . $recorrido->idComentario . ')"></i><p id="dislike' . $recorrido->idComentario . '">' . $recorrido->nogustar . '</p></td>
                                        </tr>
                                    </tbody>
                                </table>
                                
 
                            </div>
                        </div>

                    ';
                }
                return $comen;
            } else {
                header('location: login');
            }



            echo $comen;
            //echo $comentario['comentario']; 
        }
    }
    public function likesComentario()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            if (isset($_POST['idComentario'])) {
                $idComentario = $_POST['idComentario'];
                $comentario = ComentariosCentros::id($idComentario);
                // echo $comentario['idComentario'];
                $comen = $comentario['gustar'];
                // return $comen;
                echo $comen;
            } else {
                header('location: login');
            }
            //echo $comentario['comentario']; 
        }
    }
    public function dislikesComentario()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            if (isset($_POST['idComentario'])) {
                $idComentario = $_POST['idComentario'];
                $comentario = ComentariosCentros::id($idComentario);
                // echo $comentario['idComentario'];
                $comen = $comentario['nogustar'];
                // return $comen;
                echo $comen;
            } else {
                header('location: login');
            }
            //echo $comentario['comentario']; 
        }
    }
    public function formularioComentariosCentros()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            $formularioComentariosCentros =
                '
            
            <form id="formComentarioCentro" method="POST" enctype="multipart/form-data">
                <label for="titulo">Ingrese el titulo</label>
                <input class="form-control" type="text" id="titulo" name="titulo" placeholder="Ingrese el título">
                <label for="comentario">Comentario:</label>
                <input class="form-control" type="text" id="comentario" name="comentario" placeholder="Ingrese el comentario">
                <input id="btnFormComentarioCentro" onclick="saveComentarioCentro()" class="btn btn-primary" type="button" value="Guardar">
            </form>
            </div>
            
            ';
            return $formularioComentariosCentros;
        } else {
            header('location: login');
        }
    }
    public function guardarComentarioCentro()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

            if (
                !empty($_POST["titulo"]) ||
                !empty($_POST["comentario"]) ||
                !empty($_POST["idCentro"])
            ) {
            }
            $titulo = addslashes($_POST['titulo']);
            $comentario = addslashes($_POST['comentario']);
            $idCentro = addslashes($_POST['idCentro']);
            $idUsuario = $_SESSION['userId'];

            $comentarioCentro = new ComentariosCentros();
            $comentarioCentro->idCentro = $idCentro;
            $comentarioCentro->idUser = $idUsuario;
            $comentarioCentro->titulo = $titulo;
            $comentarioCentro->comentario = $comentario;
            $comentarioCentro->gustar = 0;
            $comentarioCentro->nogustar = 0;
            $comentarioCentro->fecha = date("Y-m-d H:i:s");
            $respuesta = $comentarioCentro->save();

            return $respuesta;
        } else {
            header('location: login');
        }
    }
    public function getInfoCentros()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $centros = Centro::all();
            $infoCentros = "";
            foreach ($centros as $recorrido) {
                if ($recorrido->status == 1) {
                    $infoCentros .=
                        '
                <div class="informacionCentros card">
                    <div class="card-header">
                        ' . $recorrido->nombreCentro . '
                        
                    </div>
                    <div class="card-body">
                        <p class="card-text">Horario de atención: ' . $recorrido->horario . '</p>
                        <p class="card-text">Número de contácto: ' . $recorrido->estado . '</p>
                        <p class="card-text">Tipo de centro: ' . $recorrido->tipo . '</p>
                        <a href="' . $recorrido->ubicacion . '" class="btn btn-primary">Ubicación</a>
                    </div>
                </div>
                ';
                }
            }
            return $infoCentros;
        } else {
            header('location: login');
        }
    }



    public function saveCentro()
    {
        if (
            !empty($_POST["nombreCentro"]) ||
            !empty($_POST["numero"]) ||
            !empty($_POST["estado"]) ||
            !empty($_POST["horario"]) ||
            !empty($_POST["ubicacion"]) ||
            !empty($_POST["descripcion"]) ||
            !empty($_POST["tipo"]) ||
            !empty($_POST["latitud"]) ||
            !empty($_POST["longitud"])
        ) {

            $nombreCentro = $_POST['nombreCentro'];
            $numero = $_POST['numero'];
            $estado = $_POST['estado'];
            $horario = $_POST['horario'];
            $ubicacion = $_POST['ubicacion'];
            $descripcion = $_POST['descripcion'];

            $tipo = $_POST['tipo'];
            $latitud = $_POST['latitud'];
            $longitud = $_POST['longitud'];


            $ruta_temporal = $_FILES['imagen']['tmp_name'];
            $nombreArchivo = $_FILES['imagen']['name'];
            $soloelnombre = explode('.', $nombreArchivo);
            $laextension = strtolower(end($soloelnombre));
            $extensionesValidas = array('jpeg', 'jpg', 'png');
            if (in_array($laextension, $extensionesValidas)) {
                $directorio = 'styles/images/centros/';
                $nuevoNombre = "centro_" . time() . $nombreArchivo;
                $rutaDestino = $directorio . $nuevoNombre;
                if (move_uploaded_file($ruta_temporal, $rutaDestino)) {
                    $nameExists = gestionCentroController::verifyCentroExists($nombreCentro);

                    if ($nameExists == 0) {

                        $centro = new Centro();
                        $centro->nombreCentro = addslashes($nombreCentro);
                        $centro->numero = addslashes($numero);
                        $centro->estado = addslashes($estado);
                        $centro->horario = addslashes($horario);
                        $centro->ubicacion = $ubicacion;
                        $centro->descripcion = addslashes($descripcion);
                        $centro->tipo = addslashes($tipo);
                        $centro->imagen = $rutaDestino;
                        $centro->latitud = addslashes($latitud);
                        $centro->longitud = addslashes($longitud);
                        $centro->visitas = 0;
                        $centro->status = 1;

                        $centro->createdAt = date("Y-m-d H:i:s");
                        echo $centro->save();
                    } else if ($nameExists == 1) {
                        echo 3;
                    }
                } else {
                    return 2;
                }
            } else {

                $imagen = 'styles/images/centros/centroacopio.jpg';
                $nameExists = gestionCentroController::verifyCentroExists($nombreCentro);

                if ($nameExists == 0) {

                    $centro = new Centro();
                    $centro->nombreCentro = addslashes($nombreCentro);
                    $centro->numero = addslashes($numero);
                    $centro->estado = addslashes($estado);
                    $centro->horario = addslashes($horario);
                    $centro->ubicacion = $ubicacion;
                    $centro->descripcion = addslashes($descripcion);
                    $centro->tipo = addslashes($tipo);
                    $centro->imagen = $imagen;
                    $centro->latitud = addslashes($latitud);
                    $centro->longitud = addslashes($longitud);
                    $centro->visitas = 0;
                    $centro->status = 1;

                    $centro->createdAt = date("Y-m-d H:i:s");
                    echo $centro->save();
                } else if ($nameExists == 1) {
                    echo 3;
                }
            }
        } else {
            echo "falto algún campo";
            header('Location: centros');
        }
    }

    public function agregarLike()
    {

        if (
            !empty($_POST["idComentario"])
        ) {
            // echo 'entro';
            $idComentario = addslashes($_POST['idComentario']);
            $idUsuarioActual = intval($_SESSION['userId']);
            $informacionComentario = ComentariosCentros::id($idComentario);
            $likesComentario = $informacionComentario['gustar'];
            // echo 'los likes son: '.$likesComentario;
            $informacionLikes = GustasComentarioCentro::foreignKey('idComentarioCentro', $idComentario);
            $contador = 1;
            // echo $informacionLikes['idComentarioCentro'];
            $actualizarEstatus = 0;
            if (empty($informacionLikes)) {
                $actualizarEstatus = 3;
                $contador = $contador + 1;
                $gustaComentarioCentro = new GustasComentarioCentro();
                $gustaComentarioCentro->idUser = addslashes($idUsuarioActual);
                $gustaComentarioCentro->idComentarioCentro = $idComentario;
                $gustaComentarioCentro->fecha = date("Y-m-d H:i:s");
                $gustaComentarioCentro->status = 1;
                $respuestaSaveComentario = $gustaComentarioCentro->save();
                if ($respuestaSaveComentario == 1) {
                    $likes = $likesComentario + 1;
                    $cadena = "gustar = '" . $likes . "'";
                    $actualizarLikesComentario = ComentariosCentros::update($idComentario, $cadena);
                    if ($actualizarLikesComentario == 1) {
                        echo '4';
                    } else {
                        echo 'error al actualizar la cantidad de likes 3';
                    }
                } else {
                    echo 'error al insertar nuevo like';
                }
            } else {


                foreach ($informacionLikes as $recorrido) {
                    // echo 'entro al for';
                    $idUsuariosLikes = intval($recorrido->idUser);
                    $estatusLike = intval($recorrido->status);
                    if ($idUsuariosLikes == $idUsuarioActual) {
                        $actualizarEstatus = 1;
                        if ($estatusLike == 0) {
                            $cadEstatus = "status = 1";
                            $agregarGusta = GustasComentarioCentro::updateParametro('idComentarioCentro', $idComentario, $cadEstatus);
                            $respuesta = intval($agregarGusta);
                            if ($respuesta == 1) {
                                $likes = $likesComentario + 1;
                                $cadena = "gustar = '" . $likes . "'";
                                $actualizarLikesComentario = ComentariosCentros::update($idComentario, $cadena);
                                $respuestaActLikes = intval($actualizarLikesComentario);
                                if ($respuestaActLikes == 1) {
                                    echo '1';
                                } else {
                                    echo 'error al actualizar la cantidad de likes 1';
                                }
                            } else {
                                echo 'error al actualizar el estatus 1';
                            }
                        } else if ($estatusLike == 1) {
                            $cadEstatus = "status = 0";
                            $agregarGusta = GustasComentarioCentro::updateParametro('idComentarioCentro', $idComentario, $cadEstatus);
                            $respuesta = intval($agregarGusta);
                            if ($respuesta == 1) {
                                $likes = $likesComentario - 1;
                                $cadena = "gustar = '" . $likes . "'";
                                $actualizarLikesComentario = ComentariosCentros::update($idComentario, $cadena);
                                $respuestaActLikes = intval($actualizarLikesComentario);
                                if ($respuestaActLikes == 1) {
                                    echo '2';
                                } else {
                                    echo 'error al actualizar la cantidad de likes 2';
                                }
                            } else {
                                echo 'error al actualizar el estatus 1';
                            }
                        }
                    }
                }
            }
            if ($actualizarEstatus == 0) {
                $contador = $contador + 1;
                $gustaComentarioCentro = new GustasComentarioCentro();
                $gustaComentarioCentro->idUser = addslashes($idUsuarioActual);
                $gustaComentarioCentro->idComentarioCentro = $idComentario;
                $gustaComentarioCentro->fecha = date("Y-m-d H:i:s");
                $gustaComentarioCentro->status = 1;
                $respuestaSaveComentario = $gustaComentarioCentro->save();
                if ($respuestaSaveComentario == 1) {
                    $likes = $likesComentario + 1;
                    $cadena = "gustar = '" . $likes . "'";
                    $actualizarLikesComentario = ComentariosCentros::update($idComentario, $cadena);
                    if ($actualizarLikesComentario == 1) {
                        echo '3';
                    } else {
                        echo 'error al actualizar la cantidad de likes 3';
                    }
                } else {
                    echo 'error al insertar nuevo like';
                }
            }
        } else {
            echo 'no se han recibido los parametros';
        }
    }
    public function agregardisLike()
    {

        if (
            !empty($_POST["idComentario"])
        ) {
            // echo 'entro';
            $idComentario = addslashes($_POST['idComentario']);
            $idUsuarioActual = intval($_SESSION['userId']);
            $informacionComentario = ComentariosCentros::id($idComentario);
            $dislikesComentario = $informacionComentario['nogustar'];
            // echo 'los likes son: '.$likesComentario;
            $informacionLikes = NoGustasComentarioCentro::foreignKey('idComentarioCentro', $idComentario);
            $contador = 1;
            // echo $informacionLikes['idComentarioCentro'];
            $actualizarEstatus = 0;
            if (empty($informacionLikes)) {
                $actualizarEstatus = 3;
                $contador = $contador + 1;
                $nogustaComentarioCentro = new NoGustasComentarioCentro();
                $nogustaComentarioCentro->idUser = addslashes($idUsuarioActual);
                $nogustaComentarioCentro->idComentarioCentro = $idComentario;
                $nogustaComentarioCentro->fecha = date("Y-m-d H:i:s");
                $nogustaComentarioCentro->status = 1;
                $respuestaSaveComentario = $nogustaComentarioCentro->save();
                if ($respuestaSaveComentario == 1) {
                    $likes = $dislikesComentario + 1;
                    $cadena = "nogustar = '" . $likes . "'";
                    $actualizarLikesComentario = ComentariosCentros::update($idComentario, $cadena);
                    if ($actualizarLikesComentario == 1) {
                        echo '4';
                    } else {
                        echo 'error al actualizar la cantidad de likes 3';
                    }
                } else {
                    echo 'error al insertar nuevo like';
                }
            } else {


                foreach ($informacionLikes as $recorrido) {
                    // echo 'entro al for';
                    $idUsuariosLikes = intval($recorrido->idUser);
                    $estatusLike = intval($recorrido->status);
                    if ($idUsuariosLikes == $idUsuarioActual) {
                        $actualizarEstatus = 1;
                        if ($estatusLike == 0) {
                            $cadEstatus = "status = 1";
                            $agregarGusta = NoGustasComentarioCentro::updateParametro('idComentarioCentro', $idComentario, $cadEstatus);
                            $respuesta = intval($agregarGusta);
                            if ($respuesta == 1) {
                                $likes = $dislikesComentario + 1;
                                $cadena = "nogustar = '" . $likes . "'";
                                $actualizarLikesComentario = ComentariosCentros::update($idComentario, $cadena);
                                $respuestaActLikes = intval($actualizarLikesComentario);
                                if ($respuestaActLikes == 1) {
                                    echo '1';
                                } else {
                                    echo 'error al actualizar la cantidad de likes 1';
                                }
                            } else {
                                echo 'error al actualizar el estatus 1';
                            }
                        } else if ($estatusLike == 1) {
                            $cadEstatus = "status = 0";
                            $agregarGusta = NoGustasComentarioCentro::updateParametro('idComentarioCentro', $idComentario, $cadEstatus);
                            $respuesta = intval($agregarGusta);
                            if ($respuesta == 1) {
                                $likes = $dislikesComentario - 1;
                                $cadena = "nogustar = '" . $likes . "'";
                                $actualizarLikesComentario = ComentariosCentros::update($idComentario, $cadena);
                                $respuestaActLikes = intval($actualizarLikesComentario);
                                if ($respuestaActLikes == 1) {
                                    echo '2';
                                } else {
                                    echo 'error al actualizar la cantidad de likes 2';
                                }
                            } else {
                                echo 'error al actualizar el estatus 1';
                            }
                        }
                    }
                }
            }
            if ($actualizarEstatus == 0) {
                $contador = $contador + 1;
                $nogustaComentarioCentro = new NoGustasComentarioCentro();
                $nogustaComentarioCentro->idUser = addslashes($idUsuarioActual);
                $nogustaComentarioCentro->idComentarioCentro = $idComentario;
                $nogustaComentarioCentro->fecha = date("Y-m-d H:i:s");
                $nogustaComentarioCentro->status = 1;
                $respuestaSaveComentario = $nogustaComentarioCentro->save();
                if ($respuestaSaveComentario == 1) {
                    $likes = $dislikesComentario + 1;
                    $cadena = "nogustar = '" . $likes . "'";
                    $actualizarLikesComentario = ComentariosCentros::update($idComentario, $cadena);
                    if ($actualizarLikesComentario == 1) {
                        echo '3';
                    } else {
                        echo 'error al actualizar la cantidad de likes 3';
                    }
                } else {
                    echo 'error al insertar nuevo like';
                }
            }
        } else {
            echo 'no se han recibido los parametros';
        }
    }
    public function operacionAgregarLike($likes, $idComentario, $idUsuarioActual)
    {
        $cadena = "gustar = '" . $likes . "'";
        $operacion = ComentariosCentros::update($idComentario, $cadena);
        if ($operacion == 1) {
            $gustaComentarioCentro = new GustasComentarioCentro();
            $gustaComentarioCentro->idUser = addslashes($idUsuarioActual);
            $gustaComentarioCentro->idComentarioCentro = addslashes($idComentario);
            $gustaComentarioCentro->fecha = date("Y-m-d H:i:s");
            $gustaComentarioCentro->status = 1;
            $gustaComentarioCentro->save();
            return 1;
        } else {
            return 5;
        }
    }
    public function deleteLogicoCentro()
    {

        if (
            !empty($_POST["idCentro"])
        ) {
            $idCentro = addslashes($_POST['idCentro']);
            $cadena = "status = 0";

            $respuesta = Centro::update($idCentro, $cadena);
            echo $respuesta;
        } else {
            header('Location: centros');
        }
    }

    public function createLogicoCentro()
    {

        if (
            !empty($_POST["idCentro"])
        ) {
            $idCentro = addslashes($_POST['idCentro']);
            $cadena = "status = 1";

            $respuesta = Centro::update($idCentro, $cadena);
            echo $respuesta;
        } else {
            header('Location: centros');
        }
    }

    public function deleteCentro()
    {

        if (
            !empty($_POST["idCentro"])
        ) {
            $idCentro = addslashes($_POST['idCentro']);
            $respuesta = Centro::delete($idCentro);
            echo $respuesta;
            // $respuesta = Tarjeta::update($idTarjeta, $cadena);
            // echo $cadena;


        } else {
            header('Location: centros');
        }
    }
}
