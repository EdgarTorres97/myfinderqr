<?php
//include trae todo
//unicamente la parte que necesito lo trae
require_once ROOT_PATH . '/libs/DataBase.php'; //importamos la clase database

//creando clase abstracta por que se va a utilzar para hacer herencia y no se puede instanciar.
//no se puede crear un objeto de ella
abstract class Model
{
    //creando variable estatica protegida table para decir con que tabla estoy trabajando (generica)
    protected static $table = null;
    protected static $id = null;
    protected static $rows = null;
    //creando funcion para traer todo los datos de la tabla
    public static function all()
    {
        //creando el metodo get instance
        $pdo = DataBase::getInstance();
        //accediendo a la tabla estatica table
        $sql = 'SELECT * FROM ' . static::$table;
        $resultQry = $pdo->query($sql);
        //regresando el resultado de la consulta como arreglo
        return $resultQry->fetchAll(PDO::FETCH_OBJ);
    }

    public static function consultaJSON()
    {
        try {
            //creando el metodo get instance
            $pdo = DataBase::getInstance();
            //accediendo a la tabla estatica table
            $sql = 'SELECT * FROM ' . static::$table;
            $resultQry = $pdo->prepare($sql);
            //regresando el resultado de la consulta como arreglo
            $resultQry->execute();
            $row = $resultQry->fetchAll(PDO::FETCH_OBJ);
            $pdo = Database::killInstance();
            echo json_encode($row, JSON_UNESCAPED_UNICODE);
        } catch (PDOEXCEPTION $e) {
            echo "ERROR";
        }
    }

    public static function id($id)
    {
        try {
            //creando el metodo get instance
            $pdo = DataBase::getInstance();
            //accediendo a la tabla estatica table
            // echo 'SELECT * FROM ' . static::$table . ' WHERE ' . static::$id . ' = ' . $id;
            $sql = 'SELECT * FROM ' . static::$table . ' WHERE ' . static::$id . ' = ' . $id;
            $resultQry = $pdo->query($sql);
            //regresando el resultado de la consulta como arreglo
            return $resultQry->fetch(PDO::FETCH_ASSOC);
        } catch (PDOEXCEPTION $e) {
            echo $e;
        }
    }

    public static function foreignKey($id, $foreignKey)
    {
        try {

            $pdo = DataBase::getInstance();
            //accediendo a la tabla estatica table
            // echo 'SELECT * FROM ' . static::$table . ' WHERE ' . $foreignKey . ' = ' . $id;
            $sql = 'SELECT * FROM ' . static::$table . ' WHERE ' . $foreignKey . ' = ' . $id;
            $resultQry = $pdo->query($sql);
            //regresando el resultado de la consulta como arreglo
            return $resultQry->fetchAll(PDO::FETCH_OBJ);
        } catch (PDOEXCEPTION $e) {
            echo $e;
        }
    }
    

    public function save()
    {
        $vars = get_object_vars($this);
        $keys = array_keys($vars);

        try {
            $sql = 'INSERT INTO ' . static::$table . ' (' . implode(', ', $keys) . ') VALUES (:' . implode(', :', $keys) . ')';
            $pdo = DataBase::getInstance();
            $stmt = $pdo->prepare($sql);
            $stmt->execute($vars);
            $pdo = Database::killInstance();
            return 1;
        } catch (PDOEXCEPTION $e) {
            return $e;
        }
    }

    public function update($id, $cadena)
    {
        try {
            // echo 'UPDATE ' . static::$table . ' SET '.$cadena.' WHERE '.static::$id.' = '.$id;
            $sql = 'UPDATE ' . static::$table . ' SET ' . $cadena . ' WHERE ' . static::$id . ' = ' . $id;
            $pdo = DataBase::getInstance();
            $resultQry = $pdo->prepare($sql);
            $resultQry->execute();
            $pdo = Database::killInstance();

            return 1;
        } catch (PDOEXCEPTION $e) {
            return $e;
        }
    }

    public function updateParametro($id,$parametro, $cadena)
    {
        try {
            // echo 'UPDATE ' . static::$table . ' SET '.$cadena.' WHERE '.static::$id.' = '.$id;
            $sql = 'UPDATE ' . static::$table . ' SET ' . $cadena . ' WHERE ' . $parametro . ' = ' . $id;
            $pdo = DataBase::getInstance();
            $resultQry = $pdo->prepare($sql);
            $resultQry->execute();
            $pdo = Database::killInstance();

            return 1;
        } catch (PDOEXCEPTION $e) {
            return $e;
        }
    }

    public function delete($id)
    {
        try {
            $sql = 'DELETE FROM ' . static::$table . ' WHERE ' . static::$id . ' = ' . $id;
            $pdo = DataBase::getInstance();
            $resultQry = $pdo->prepare($sql);
            $resultQry->execute();
            $pdo = Database::killInstance();
            return 1;
        } catch (PDOEXCEPTION $e) {
            return 2;
        }
    }

    public function createTable($nombreTabla, $campo)
    {
        try {
            // echo 'CREATE TABLE IF NOT EXISTS ' . $nombreTabla . ' ( id' . $nombreTabla . ' BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT, ' . $campo . ' )';
            $sql = 'CREATE TABLE IF NOT EXISTS ' . $nombreTabla . ' ( id' . $nombreTabla . ' BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT, ' . $campo . ' )';
            $pdo = DataBase::getInstance();
            $resultQry = $pdo->prepare($sql);
            $resultQry->execute();
            $pdo = Database::killInstance();
            return 1;
        } catch (PDOEXCEPTION $e) {
            return $e;
        }
    }
}
