    <?php
    class DataBase extends PDO
    {
        
        //creando variable privada instance
        private static $instance = null;
        //constructor
        //options=[]]
        public function __construct($server, $username, $pass, $options = [])
        {
            //accediendo al constructor del padre
            parent::__construct($server, $username, $pass, $options);
        }
        public static function getInstance()
        {
            //va a preguntar si ya tenemos una instancia, si no la tiene la va a crear
            if (is_null(self::$instance)) { //
                //creando instancia
                self::$instance = new DataBase('mysql:host=localhost;dbname=mvc;charset=utf8', 'root', '');
                //self::$instance = new DataBase("sqlsrv:Server=localhost;Database=tramites", "usuario", "12345");

                //agregandole un atributo a la instancia
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            //regresando instancia
            return self::$instance;
        }
        public static function killInstance()
        {
            $instance = null;
        }
    }
