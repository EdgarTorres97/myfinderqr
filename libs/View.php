<?php
//creando vista
class View
{
    //creando el template html
    protected $template;
    public function __construct($template = null, $params = [])
    {
        //recibiendo un elemento 
        $this->setTemplate($template);
        $this->setParams($params);
    }
    //creando función template que es recibida de la variable template
    public function setTemplate($valTemplate)
    {
        //haciendo validacion

        if (isset($valTemplate)) { //isset es para saber si una variable existe y no es null
            //asiganando el valor que estamos mostrando como parametro
            $this->template = $valTemplate;
        }
    }
    //creando la funcion setparams
    public function setParams($valParams)
    {
        //verificando sin valparams es un arreglo
        if (is_array($valParams)) {
            //arreglo en donde se renombra la variable y el valor que tenga ese alias se va a llamar value
            foreach ($valParams as $key => $value) {
                //por cada elemento que tenga ese arreglo debe atrapar el valor.
                $this->{$key} = $value;
            }
        }
    }
    public function render()
    {
        require_once ROOT_PATH . '/views/' . $this->template . '.php';
    }
    public function __toString()
    {
        ob_start();
        $this->render();
        return ob_get_clean();
    }
}
