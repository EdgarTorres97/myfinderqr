<?php
require_once ROOT_PATH . '/libs/Model.php';

class NoGustasComentarioCentro extends Model
{
    static $table = 'nogustasComentarioCentro';

    static $id = 'idnoGustaComentarioCentro';

    static $rows = [
        'idUser',
        'idComentarioCentro',
        'status',
        'fecha'
    ];
}
