<?php
require_once ROOT_PATH. '/libs/Model.php';

class Home extends Model{
    static $table='home';

    static $rows = ['idHome','description'];
}