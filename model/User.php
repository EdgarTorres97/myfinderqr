<?php
require_once ROOT_PATH. '/libs/Model.php';

class User extends Model{
    static $table='Users';
    static $id = 'idUser';
    static $rows = ['idUser','name','lastName','age','mobile','email','password','status','createdAt'];
}