<?php
require_once ROOT_PATH . '/libs/Model.php';

class Tarjeta extends Model
{
    static $table = 'tarjetas';

    static $id = 'idTarjeta';

    static $rows = [
        'nombreUsuario',
        'numeroTarjeta',
        'tipo',
        'mesVencimiento',
        'anioVencimiento',
        'codigoSeguridad',
        'fechaAlta'
    ];
}
