<?php
require_once ROOT_PATH . '/libs/Model.php';

class GustasComentarioCentro extends Model
{
    static $table = 'gustasComentarioCentro';

    static $id = 'idGustaComentarioCentro';

    static $rows = [
        'idUser',
        'idComentarioCentro',
        'status',
        'fecha'
    ];
}
