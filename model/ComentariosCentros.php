<?php
require_once ROOT_PATH . '/libs/Model.php';

class ComentariosCentros extends Model
{
    static $table = 'comentariosCentros';

    static $id = 'idComentario';

    static $rows = [
        'idCentor',
        'comentario',
        'fecha'
    ];
}
