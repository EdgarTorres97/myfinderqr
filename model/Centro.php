<?php
require_once ROOT_PATH . '/libs/Model.php';

class Centro extends Model
{
    static $table = 'centros';

    static $id = 'idCentro';

    static $rows = [
        'nombreCentro',
        'numero',
        'estado',
        'horario',
        'ubicacion',
        'tipo',
        'latitud',
        'longitud',
        'createdAt'
    ];
}
